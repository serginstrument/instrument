<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInstrumentImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instrument_images', function (Blueprint $table) {
            $table->bigIncrements ('id');
            $table->string('img');
            $table->bigInteger('instrument_id')->unsigned ();
            $table->foreign('instrument_id')
                ->references ('id')
                ->on ('instruments')
                ->onDelete ('cascade');
            $table->timestamps ();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('instrument_images');
    }
}
