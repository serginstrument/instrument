<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('instrument_order', function (Blueprint $table) {
            $table->unsignedBigInteger('instrument_id');
            $table->string('first_name')->after('id');
            $table->string('last_name')->after('id');
            $table->unsignedBigInteger('order_id');
            $table -> integer ('count') -> default (1)->after ('order_id');
            $table->timestamps();

            $table->foreign('instrument_id')
                ->references('id')
                ->on('instruments')->onDelete('cascade');

            $table->foreign('order_id')
                ->references('id')
                ->on('orders');

            $table->primary(['instrument_id','order_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('instrument_order');
    }
};
