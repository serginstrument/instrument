<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoryInstrumentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category_instrument', function (Blueprint $table) {
            $table->unsignedBigInteger('instrument_id');
            $table->unsignedBigInteger('category_id');

            $table->foreign('instrument_id')
                ->references('id')
                ->on('instruments')->onDelete('cascade');

            $table->foreign('category_id')
                ->references('id')
                ->on('categories');

            $table->primary(['instrument_id','category_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category_instrument');
    }
}
