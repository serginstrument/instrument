<?php

namespace Database\Factories;


use App\Models\Instrument;
use App\Models\Manufacture;
use Illuminate\Database\Eloquent\Factories\Factory;

class InstrumentFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    protected $model = Instrument::class;

    public function definition(): array
    {
        return [
            'name' => $this->faker->name(),
            'manufacture_id' => Manufacture::get()->random()->id,
            'price' => random_int(1,500),
            'new_price' => random_int(1,500),
            'description' => $this->faker->text(),
            'leftovers' => random_int(1,100),

            ];
    }
}
