<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Category;
use App\Models\Instrument;
use App\Models\Manufacture;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        Manufacture::factory(10)->create();
        $categories = Category::factory(10)->create();
        $instruments = Instrument::factory(50)->create();

        foreach($instruments as $instrument){
            $categoriesIds = $categories->random(5)->pluck('id');
            $instrument->categories()->attach($categoriesIds);
        }

        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);
    }
}
