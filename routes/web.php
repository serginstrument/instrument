<?php

use App\Http\Controllers\Admin\InstrumentExportController;
use App\Http\Controllers\Admin\InstrumentImportController;
use App\Http\Controllers\Admin\ManufactureController;
use App\Http\Controllers\Admin\OrderController;
use App\Http\Controllers\Admin\InstrumentController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\PermissionController;
use App\Http\Controllers\Admin\RoleController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\UserExportController;
use App\Http\Controllers\Admin\UserImportController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\CartController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/telescope', function () {
    return view('telescope');
});


Route::get('locale/{locale}', 'LocaleController@changeLocale')->name('locale');
Route::get('currency/{currencyCode}', 'CurrencyController@changeCurrency')->name('currency');

Route::middleware(['set_locale'])->group(function (){

    Route::name('user.')->group(function(){

        Route::get('/login', function(){
            if(Auth::check()){
                return redirect(route ('instrument.index'));
            }
            return view('login');
        })->name('login');

        Route::post('/login',[LoginController::class, 'login']);

        Route::get('/forgot', [LoginController::class, 'showForgotForm'])->name('forgot');
        Route::post('/forgot_process', [LoginController::class, 'forgot'])->name('forgot_process');

        Route::get('/logout',function(){
            Auth::logout();
            return redirect(route('instrument.index'));
        })->name('logout');

        Route::get('/registration',function (){
            if(Auth::check()){
                return redirect(route('instrument.index'));
            }
            return view('registration');
        })->name('registration');
        Route::post('/registration',[RegisterController::class, 'save']);
    });

    Route::get ('/category', [App\Http\Controllers\CategoryController::class, 'index'])->name('category.index');
    Route::get ('/category/{category}', [App\Http\Controllers\CategoryController::class, 'show'])->name('category.show');

    Route::get ('/instrument', [App\Http\Controllers\InstrumentController::class, 'index'])->name('instrument.index');
    Route::get ('/instrument/{instrument}', [App\Http\Controllers\InstrumentController::class, 'show'])->name('instrument.show');

    Route::get('/cart', [CartController::class, 'index'])->name('cart.index');
    Route::post('/cart/add/{instrument}', [CartController::class, 'cartAdd'])->name('cart-add');
    Route::post('/cart/remove/{instrument}', [CartController::class, 'cartRemove'])->name('cart-remove');
    Route::get('/cart/place', [CartController::class, 'cartPlace'])->name('cart.place');
    Route::post('/cart/place', [CartController::class, 'cartConfirm'])->name('cart.confirm');

});

Route::middleware(['role:admin'])->name('admin.')->prefix('admin_panel')->group(function () {

    Route::get ('/',function (){
        return view('admin.admin_panel');
    });
    Route ::resource ('/instrument', InstrumentController::class);
    Route ::resource ('/category', CategoryController::class);
    Route ::resource ('/order', OrderController::class);
    Route ::resource ('/manufacture', ManufactureController::class);
    Route ::resource ('/user', UserController::class);
    Route ::resource ('/permission', PermissionController::class);
    Route ::resource ('/role', RoleController::class);

});

Route::get('admin/orders/{id}/send', [OrderController::class, 'sendOrderTableToCustomer'])->name('admin.orders.send');

Route::post('admin/import-instruments', [InstrumentImportController::class, 'import'])->name('admin.import-instruments');
Route::get('admin/export-instruments', [InstrumentExportController::class, 'export'])->name('admin.export-instruments');

Route::post('admin/import-users', [UserImportController::class, 'import'])->name('admin.import-users');
Route::get('admin/export-users', [UserExportController::class, 'export'])->name('admin.export-users');
