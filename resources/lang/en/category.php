<?php

return [
    'button_go_to_category' => 'Go to category',
    'free_shipping_worldwide' => 'Free Shipping Worldwide',
    'free_returns' => 'Free Returns',
    'fast_support' => '24h Fast Support',
    'subscribe' => 'Subscribe to our newsletter',
    'button_subscribe' => 'Subscribe',
    'in_stock' => 'In stock',
    'unavailable' => 'Unavailable',
    'showing' => "Showing",
    'results' => "results",
    'sort' => 'Sort by',

];
