<?php

return [
    'instruments_shop' => 'Instruments shop',
    'accessories' => 'Accessories',
    'categories' => 'Categories',
    'current_lang' => 'en',
    'set_lang' => 'ukr',
    'cart' => 'Cart',
    'instrument' => 'Instrument',
    'home' => 'Home',
    'contact' => 'Contact',
    'log_in' => 'Log in',
    'log_out' => 'Log out',
    'admin' => 'Admin',
];
