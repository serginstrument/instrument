<?php

return [
    'experience' => 'A new Online Shop experience.',
    'description' => 'The construction tool is your reliable partner in every construction project.
    With us, building becomes easier, faster and more efficient!',
    'amazing_tools' => 'Amazing Tools',
    'see_more' => 'See More',
    'free_shipping_worldwide' => 'Free Shipping Worldwide',
    'free_returns' => 'Free Returns',
    'fast_support' => '24h Fast Support',
    'subscribe' => 'Subscribe to our newsletter',
    'button_subscribe' => 'Subscribe',
    'in_stock' => 'In stock',
    'unavailable' => 'Unavailable',
    'search_instrument' => 'Search instrument',
    'availability:' => 'Availability:',
    'add_to_cart' => 'Add to cart',
    'description_instrument_show' => 'Description',
    'share' => 'Share',
    'reviews' => 'Reviews',
    'specifications' => 'Specifications',
    'related_products' => 'Related Products',
    'manufacture' => 'Manufacture',
    'category' => 'Category',
    'guaranty' => 'Guaranty',

];
