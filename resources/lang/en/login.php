<?php

return [

    'entering' => 'Entering',
    'email' => 'Email',
    'password' => 'Password',
    'forgot_password' => 'Forgot password?',
    'sign_in' => 'Sign in',
    'registration' => 'Registration',
    'enter_email' => 'Enter email',
    'enter_password' => 'Enter password',
    'first_name' => 'First name',
    'last_name' => 'Last name',
    'repeat_password' => 'Repeat password',
    'enter_first_name' => 'Enter first name',
    'enter_last_name' => 'Enter last name',
    'confirm_password' => 'Repeat password',
    'sign_up' => 'Sign up',
    'send' => 'Send',
];
