<?php

return [
    'instruments_shop' => 'Магазин інструменту',
    'accessories' => 'Аксесуари',
    'categories' => 'Категорії',
    'current_lang' => 'ukr',
    'set_lang' => 'en',
    'cart' => 'Кошик',
    'instrument' => 'Інструмент',
    'home' => 'Домашня',
    'contact' => 'Контакти',
    'log_in' => 'Увійти',
    'log_out' => 'Вийти',
    'admin' => 'Адмін',
];
