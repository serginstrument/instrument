<?php

return [

    'button_go_to_category' => 'Перейти в категорію',
    'free_shipping_worldwide' => 'Безкоштовна доставка по всьому світу',
    'free_returns' => 'Безкоштовне повернення',
    'fast_support' => '24г Швидка підтримка',
    'subscribe' => 'Підпишіться на наші новини',
    'button_subscribe' => 'Підписатися',
    'in_stock' => 'В наявності',
    'unavailable' => 'Немає в наявності',
    'showing' => "Знайдено",
    'results' => "результатів",
    'sort' => 'Сортувати',

];
