<?php

return [
    'experience' => 'Новий досвід інтернет-магазину.',
    'description' => 'Будівельний інструмент - ваш надійний партнер у кожному будівельному проекті.
     З нами будувати стає простіше, швидше і ефективніше!',
    'amazing_tools' => 'Дивовижні інструменти',
    'see_more' => 'Бачити більше',
    'free_shipping_worldwide' => 'Безкоштовна доставка по всьому світу',
    'free_returns' => 'Безкоштовне повернення',
    'fast_support' => '24г Швидка підтримка',
    'subscribe' => 'Підпишіться на наші новини',
    'button_subscribe' => 'Підписатися',
    'in_stock' => 'В наявності',
    'unavailable' => 'Немає в наявності',
    'search_instrument' => 'Шукати інструмент',
    'availability' => 'Доступність:',
    'add_to_cart' => 'Додати в кошик',
    'description_instrument_show' => 'Опис',
    'share' => 'Поділитися',
    'reviews' => 'Відгуки',
    'specifications' => 'Спеціфікація',
    'related_products' => 'Супутні товари',
    'manufacture' => 'Виробник',
    'category' => 'Категорія',
    'guaranty' => 'Гарантія',
];
