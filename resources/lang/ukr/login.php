<?php

return [

    'entering' => 'Вхід',
    'email' => 'Електронна пошта',
    'password' => 'Пароль',
    'forgot_password' => 'Забули пароль?',
    'sign_in' => 'Увійти',
    'registration' => 'Реєстрація',
    'enter_email' => 'Введіть пошту',
    'enter_password' => 'Введіть пароль',
    'first_name' => "Ім'я",
    'last_name' => 'Прізвище',
    'repeat_password' => 'Підтвердіть пароль',
    'enter_first_name' => "Введіть ім'я",
    'enter_last_name' => 'Введіть прізвище',
    'confirm_password' => 'Підтвердіть пароль',
    'sign_up' => 'Зареєструватися',
    'send' => 'Відправити',
];
