@extends('layouts.main')

@section('title', 'log in')
@section('custom_css')
    <link rel="stylesheet" type="text/css" href="/styles/product.css">
    <link rel="stylesheet" type="text/css" href="/styles/product_responsive.css">
@endsection

@section('content')
    <div class="home">

      <section class="content">
         <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <h3 class="m-0">@lang('login.entering')</h3>
                    <div class="card card-primary">
                        <!-- form start -->
                        <form action="{{route('user.login')}}" method="POST">
                            @csrf

                            <div class="card-body">
                                <div class="form-group">
                                    <label for="email">@lang('login.email')</label>
                                    <input type="text"
                                           name="email"
                                           value="{{old('email')}}" id="email"
                                           placeholder="@lang('login.enter_email')" class="form-control">
                                    @error('email')
                                    <div class="alert alert-danger"> {{$message}}</div>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label for="password">@lang('login.password')</label>
                                    <input type="password"
                                           name="password"
                                           value="" id="password"
                                           placeholder="@lang('login.enter_password')"
                                           class="form-control">
                                    @error('password')
                                    <div class="alert alert-danger"> {{$message}}</div>
                                    @enderror
                                </div>
                            <div>
                                <a href="{{ route ("user.forgot") }}" class="me-3 py-2 text-dark text-decoration-none"
                                   style="font-size:12px">@lang('login.forgot_password')</a>
                            </div>
                            </br>
                            <button type="submit" class="btn btn-success">@lang('login.sign_in')</button>
                            <a class="me-3 py-2 text-dark text-decoration-none" href="{{route ('user.registration')}}">@lang('login.registration')</a>
                    </div>
                            </br>
                        </form>
                    </div>
                </div>
            </div>
        </div>
      </section>
      </br>
    </div>

@endsection
