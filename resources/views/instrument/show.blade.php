@extends('layouts.main')

@section('title', $instrument->name)
@section('custom_css')
    <link rel="stylesheet" type="text/css" href="/styles/product.css">
    <link rel="stylesheet" type="text/css" href="/styles/product_responsive.css">
@endsection

@section('custom_js')
    <script src="/js/product.js"></script>
    <script>
        $(document).ready(function () {
            $('.cart_button').click(function (event) {
                event.preventDefault()
                addToCart()
            })
        })

        function addToCart() {
            let id = $('.details_name').data('id')
            let qty = parseInt($('#quantity_input').val())
            let total_qty = parseInt($('.cart-qty').text())
            total_qty += qty
            $('.cart-qty').text(total_qty)
            $.ajax({
                url: "#",
                type: "POST",
                data: {
                    id: id,
                    qty: qty,
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: (data) => {
                    console.log(data)
                },
                error: (data) => {
                    console.log(data)
                }
            });
        }
    </script>
@endsection

@section('content')

    <!-- Home -->

    <div class="home">
        <div class="home_container">
            <div class="home_background"
                 style="background-image:url(https://st2.depositphotos.com/1158226/6622/i/950/depositphotos_66227973-stock-photo-saw-hammer-and-planer-in.jpg)"></div>
            <div class="home_content_container">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <div class="home_content">
                                <div class="home_title">Carpentry<span>.</span></div>
                                <div class="home_text"><p>In general, carpentry works include everything that is
                                        associated with the manufacture of individual products made of wood or mainly
                                        wood, but not carrying a structural load. Also does not apply to carpentry and
                                        artistic work with wood, such as woodcarving. But the manufacture of balusters
                                        and other things on a lathe is quite the prerogative of the carpenter.</p></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Product Details -->

    <div class="product_details">
        <div class="container">
            <div class="row details_row">
                <div class="col-lg-6">
                    <div class="details_image">
                        @php
                            $image = '';
                            if(count($instrument->images) > 0){
                                $image = $instrument->images[0]['img'];
                            } else {
                                $image = "/images/no_image.jpg";
                            }
                        @endphp
                        <div class="details_image_large" data-image="/storage/ . $image}"><img
                                src="{{ count($instrument->images) > 0 ? '/storage/'.$image : '/images/no_image.jpg' }}"
                                width="400" height="400" alt="">

                            <div class="product_extra product_new"><a href="#"
                                                                      style=" background-color: #DEB887">{{$instrument['manufacture']['name']}}</a>
                            </div>
                        </div>
                        <div class="details_image_thumbnails d-flex flex-row align-items-start justify-content-between">
                            @if($image == '/images/no_image.jpg')
                            @else
                                @foreach($instrument->images as $img)
                                    @if($loop->first)
                                        <div class="details_image_thumbnail active"
                                             data-image="/storage/{{$img['img']}}"><img
                                                src="{{ asset('/storage/' . $img['img']) }}"
                                                alt="{{$instrument->name}}"
                                                width="100" height="100"></div>
                                    @else
                                        <div class="details_image_thumbnail"
                                             data-image="/storage/{{$img['img']}}"><img
                                                src="{{ asset('/storage/' . $img['img']) }}"
                                                alt="{{$instrument->name}}"
                                                width="100" height="100"></div>
                                    @endif
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>

                <!-- Product Content -->

                <div class="col-lg-6">
                    <div class="details_content">
                        <div class="details_name" data-id="{{$instrument->id}}">{{$instrument->__('name')}}</div>
                        @if($instrument->new_price != null)
                            <div class="details_discount">{{ number_format($instrument->price, 2, '.', ' ') }}
                                {{ App\Services\CurrencyConvertionService::getCurrencySymbol() }}
                            </div>
                            <div class="details_price"
                                 style="font-weight:inherit;  font-size: 20px; color:#3CB371">{{ number_format($instrument->new_price, 2, '.', ' ') }}
                                {{ App\Services\CurrencyConvertionService::getCurrencySymbol() }}
                            </div>
                        @else
                            <div class="details_price"
                                 style="font-weight:inherit;  font-size: 20px; color:#3CB371">{{ number_format($instrument->price, 2, '.', ' ') }}
                                {{ App\Services\CurrencyConvertionService::getCurrencySymbol() }}
                            </div>
                        @endif

                        <!-- In Stock -->

                        <div class="in_stock_container">
                            <div class="availability">@lang('instrument.availability')</div>
                            @if($instrument->leftovers > 0)
                            <span>@lang('instrument.in_stock')</span>
                            @else
                            <span style="color: #cc0000">@lang('instrument.unavailability')</span>
                            @endif
                        </div>
                        <div class="details_text">
                            <p style="color: black">{{$instrument->__('description')}}</p>
                        </div>

                        <form action="{{ route('cart-add', $instrument) }}" method="POST" id="add-to-cart-form">
                            @csrf
                            @if($instrument->leftovers > 0)
                            <div class="product_quantity_container">
                                <div class="product_quantity clearfix">
                                    <span>Qty</span>
                                    <input id="quantity_input" type="text" pattern="[0-9]*" value="1" name="quantity">
                                    <div class="quantity_buttons">
                                        <div id="quantity_inc_button" class="quantity_inc quantity_control"
                                             onclick="increaseQuantity()">
                                            <i class="fa fa-chevron-up" aria-hidden="true"></i>
                                        </div>
                                        <div id="quantity_dec_button" class="quantity_dec quantity_control"
                                             onclick="decreaseQuantity()"><i class="fa fa-chevron-down"
                                                                             aria-hidden="true"></i></div>
                                    </div>
                                </div>
                                <script>
                                    function increaseQuantity() {
                                        var quantityInput = document.getElementById('quantity_input');
                                        var currentQuantity = parseInt(quantityInput.value);
                                        quantityInput.value = currentQuantity - 1;
                                    }

                                    function decreaseQuantity() {
                                        var quantityInput = document.getElementById('quantity_input');
                                        var currentQuantity = parseInt(quantityInput.value);
                                        if (currentQuantity > 1) {
                                            quantityInput.value = currentQuantity
                                                + 1;
                                        }
                                    }
                                </script>

                                <button type="submit" class="btn btn-outline-success btn-lg mt-2" role="button" id="add-to-cart-btn">
                                    @lang('instrument.add_to_cart')
                                </button>


                            </div>

                            @else
                                <br>
                            <span style="font-size: larger;">Сontact our manager if you want to order this product</span>

                            @endif
                        </form>

                        <div class="details_share">
                            <span>@lang('instrument.share'):</span>
                            <ul>
                                <li><a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row description_row">
                <div class="col">
                    <div class="description_title_container">
                        <div class="description_title">@lang('instrument.description_instrument_show')</div>
                        <div class="reviews_title"><a href="#">@lang('instrument.reviews') <span>(1)</span></a></div>
                    </div>
                    <div class="description_text">
                        <h3>@lang('instrument.specifications')</h3>
                    </div>
                </div>
            </div>

            <table style="width: 50%">
                <tr>
                    <th>@lang('instrument.manufacture')</th>
                    <td>TOPEX</td>
                </tr>
                <tr>
                    <th>@lang('instrument.category')</th>
                    <td>Carpentry</td>
                </tr>
                <tr>
                    <th>@lang('instrument.guaranty')</th>
                    <td>Six month</td>
                </tr>
            </table>
        </div>
    </div>

    <style>
        .product {
            width: auto;
            /*border: 1px solid #ddd;*/
        }
    </style>
    <div class="products">
        <div class="container" style="margin: auto; width: 140%;">
            <div class="row">
                <div class="col text-center">
                    <div class="products_title">@lang('instrument.related_products')</div>
                </div>
            </div>
            <div class="row">
                @foreach($recommendedInstruments as $recommendedInstrument)
                    <div class="col-md-3">
                        <div class="product">
                            <div class="product_image">
                                <a href="{{ route('instrument.show', $recommendedInstrument) }}">
                                    @if($recommendedInstrument->images->count() > 0)
                                        <img src="{{ asset('/storage/'.$recommendedInstrument->images->first()->img) }}"
                                             alt="{{ $recommendedInstrument->name }}" width="200" height="200">
                                    @else
                                        <img src="{{ asset('/images/no_image.jpg') }}"
                                             alt="{{ $recommendedInstrument->name }}" width="200" height="200">
                                    @endif
                                </a>
                            </div>
                            <div class="product_content">
                                <div class="product_title" style="font-size: 10px;">
                                    <a href="{{ route('instrument.show', $recommendedInstrument) }}">
                                        {{ $recommendedInstrument->name }}
                                    </a>
                                </div>
                                @if($recommendedInstrument->new_price != null)
                                    <div
                                        class="details_discount">{{ number_format($recommendedInstrument->price, 2, '.', ' ') }}
                                        {{ App\Services\CurrencyConvertionService::getCurrencySymbol() }}
                                    </div>
                                    <div class="details_price"
                                         style="font-weight:inherit;  font-size: 20px; color:#3CB371">{{ number_format($recommendedInstrument->new_price, 2, '.', ' ') }}
                                        {{ App\Services\CurrencyConvertionService::getCurrencySymbol() }}
                                    </div>
                                @else
                                    <div class="details_price"
                                         style="font-weight:inherit;  font-size: 20px; color:#3CB371">{{ number_format($recommendedInstrument->price, 2, '.', ' ') }}
                                        {{ App\Services\CurrencyConvertionService::getCurrencySymbol() }}
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

    <!-- Newsletter -->

    <div class="newsletter">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="newsletter_border"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <div class="newsletter_content text-center">
                        <div class="newsletter_title">@lang('instrument.subscribe')</div>
                        <div class="newsletter_text"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam
                                a ultricies metus. Sed nec molestie eros</p></div>
                        <div class="newsletter_form_container">
                            <form action="#" id="newsletter_form" class="newsletter_form">
                                <input type="email" class="newsletter_input" required="required">
                                <button class="newsletter_button trans_200"><span>@lang('instrument.button_subscribe')</span></button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
