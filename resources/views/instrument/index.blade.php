@extends('layouts.main')

@section('title', 'More than 20k tool items')
@section('custom_css')
    <link rel="stylesheet" type="text/css" href="/styles/product.css">
    <link rel="stylesheet" type="text/css" href="/styles/product_responsive.css">
@endsection

@section('content')

    <div class="home">
        <div class="home_slider_container">

            <!-- Home Slider -->
            <div class="owl-carousel owl-theme home_slider">

                <!-- Slider Item -->
                <div class="owl-item home_slider_item">
                    <div class="home_slider_background"
                         style="background-image:url(https://st2.depositphotos.com/1158226/6622/i/950/depositphotos_66227973-stock-photo-saw-hammer-and-planer-in.jpg)"></div>
                    <div class="home_slider_content_container">
                        <div class="container">
                            <div class="row">
                                <div class="col">
                                    <div class="home_slider_content" data-animation-in="fadeIn"
                                         data-animation-out="animate-out fadeOut">
                                        <div class="home_slider_title">@lang('instrument.experience')</div>
                                        <div class="home_slider_subtitle">@lang('instrument.description')
                                        </div>
                                        <div class="button button_light home_button"><a href="#">Shop Now</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Slider Item -->
                <div class="owl-item home_slider_item">
                    <div class="home_slider_background"
                         style="background-image:url(https://zdesinstrument.ru/wp-content/uploads/2017/11/Sadovyiy-instrument-2.jpg)"></div>
                    <div class="home_slider_content_container">
                        <div class="container">
                            <div class="row">
                                <div class="col">
                                    <div class="home_slider_content" data-animation-in="fadeIn"
                                         data-animation-out="animate-out fadeOut">
                                        <div class="home_slider_title">@lang('instrument.experience')</div>
                                        <div class="home_slider_subtitle">@lang('instrument.description')
                                        </div>
                                        <div class="button button_light home_button"><a href="#">Shop Now</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Slider Item -->
                <div class="owl-item home_slider_item">
                    <div class="home_slider_background"
                         style="background-image:url(https://c.dns-shop.ru/thumb/st4/fit/760/600/8e67bd1a97ce7b7f8e24ef51c6563d70/q93_dd77dd50cd9725e41d287b03360128046a51c36aa5b19e1f91919713b45852a1.jpg.webp)"></div>
                    <div class="home_slider_content_container">
                        <div class="container">
                            <div class="row">
                                <div class="col">
                                    <div class="home_slider_content" data-animation-in="fadeIn"
                                         data-animation-out="animate-out fadeOut">
                                        <div class="home_slider_title">@lang('instrument.experience')</div>
                                        <div class="home_slider_subtitle">@lang('instrument.description')
                                        </div>
                                        <div class="button button_light home_button"><a href="#">Shop Now</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Home Slider Dots -->

            <div class="home_slider_dots_container">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <div class="home_slider_dots">
                                <ul id="home_slider_custom_dots" class="home_slider_custom_dots">
                                    <li class="home_slider_custom_dot active">01.</li>
                                    <li class="home_slider_custom_dot">02.</li>
                                    <li class="home_slider_custom_dot">03.</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="products">
        <div class="container">
            <form action="{{route('instrument.index')}}" method="get"
                  class="d-flex align-items-center justify-content-end w-50">
                <div class="input-group">
                    <input name="search_name" @if(isset($_GET['search_name'])) value="{{$_GET['search_name']}}"
                           @endif type="text" class="form-control" placeholder="@lang('instrument.search_instrument')"
                           aria-label="Search">
                </div>
            </form>
        </div>

        <style>
            .product:hover {
                box-shadow: 0 0 2px rgba(0, 0, 0, 0.3);
                border: 1px solid green;
            }

            .product_extra a.topex {
                background-color: #F0E68C;
            }

            .product_extra a.fit {
                background-color: #228B22;
            }

            .product_extra a.mtx {
                background-color: #DC143C;
            }

            .product_extra a.sigma {
                background-color: #ADD8E6;
            }

            .product_extra a.dnipro {
                background-color: #FF8C00;
            }

        </style>

        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="product_grid">
                        @foreach($instruments as $instrument)
                            @php
                                $image = '';
                                if (count($instrument->images) > 0){
                                    $image = $instrument->images[0]['img'];
                                } else {
                                $image = '/images/no_image.jpg';
                                }
                            @endphp
                            <div class="product" style="
                             padding: 20px ;  background-color: #F8F8FF;">
                                <div class="product_image">
                                    <a href="{{route('instrument.show', $instrument)}}">
                                        <img
                                            src="{{ count($instrument->images) > 0 ? '/storage/'.$image : '/images/no_image.jpg' }}"
                                            width="200" height="250" alt="" class="card-img-top">
                                    </a>
                                </div>
                                <div class="product_extra product_new">
                                    <a href="#"
                                       class="{{ strtolower(str_replace(' ', '', $instrument['manufacture']['name'])) }}">
                                        {{ $instrument['manufacture']['name'] }}
                                    </a>
                                </div>
                                <div class="product_content">
                                    <div class="product_title"
                                         style=" white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
                                        <a href="{{route('instrument.show', $instrument)}}"
                                           style="font-weight:lighter; font-size:1.2em; color:darkslategray">{{ $instrument->__('name')}}</a>
                                    </div>

                                    @if($instrument->new_price != null)
                                        <div
                                            class="details_discount">{{ number_format($instrument->price, 2, '.', ' ') }}{{ App\Services\CurrencyConvertionService::getCurrencySymbol() }}</div>
                                        <div class="details_price"
                                             style="font-weight:inherit;  font-size: 20px; color:#3CB371">{{ number_format($instrument->new_price, 2, '.', ' ') }}{{ App\Services\CurrencyConvertionService::getCurrencySymbol() }}</div>
                                    @else
                                        <div class="details_price"
                                             style="font-weight:inherit;  font-size: 20px; color:#3CB371">{{ number_format($instrument->price, 2, '.', ' ') }}{{ App\Services\CurrencyConvertionService::getCurrencySymbol() }}</div>
                                    @endif

                                </div>
                                @if($instrument->leftovers > 0)
                                    <span style="color: green">@lang('instrument.in_stock')</span>
                                @else
                                    <span>@lang('instrument.unavailable')</span>
                                @endif
                            </div>
                        @endforeach
                    </div>
                    <div class="mt-3">
                        {{$instruments->withQueryString()->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="avds_xl">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="avds_xl_container clearfix">
                        <div class="avds_xl_background"
                             style="background-image:url(https://best-stroy.ru/foto-statji/57/03/malyarnyy-instrument-lg.jpg?1513587163)"></div>
                        <div class="avds_xl_content">
                            <div class="avds_title">@lang('instrument.amazing_tools')</div>
                            <div class="avds_text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam a
                                ultricies metus.
                            </div>
                            <div class="avds_link avds_xl_link"><a
                                    href="categories.html">@lang('instrument.see_more')</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Icon Boxes -->

    <div class="icon_boxes">
        <div class="container">
            <div class="row icon_box_row">

                <!-- Icon Box -->
                <div class="col-lg-4 icon_box_col">
                    <div class="icon_box">
                        <div class="icon_box_image"><img src="images/icon_1.svg" alt=""></div>
                        <div class="icon_box_title">@lang('instrument.free_shipping_worldwide')</div>
                        <div class="icon_box_text">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam a ultricies metus. Sed
                                nec molestie.</p>
                        </div>
                    </div>
                </div>

                <!-- Icon Box -->
                <div class="col-lg-4 icon_box_col">
                    <div class="icon_box">
                        <div class="icon_box_image"><img src="images/icon_2.svg" alt=""></div>
                        <div class="icon_box_title">@lang('instrument.free_returns')</div>
                        <div class="icon_box_text">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam a ultricies metus. Sed
                                nec molestie.</p>
                        </div>
                    </div>
                </div>

                <!-- Icon Box -->
                <div class="col-lg-4 icon_box_col">
                    <div class="icon_box">
                        <div class="icon_box_image"><img src="images/icon_3.svg" alt=""></div>
                        <div class="icon_box_title">@lang('instrument.fast_support')</div>
                        <div class="icon_box_text">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam a ultricies metus. Sed
                                nec molestie.</p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <!-- Newsletter -->

    <div class="newsletter">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="newsletter_border"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <div class="newsletter_content text-center">
                        <div class="newsletter_title">@lang('instrument.subscribe')</div>
                        <div class="newsletter_text"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam
                                a ultricies metus. Sed nec molestie eros</p></div>
                        <div class="newsletter_form_container">
                            <form action="#" id="newsletter_form" class="newsletter_form">
                                <input type="email" class="newsletter_input" required="required">
                                <button class="newsletter_button trans_200">
                                    <span>@lang('instrument.button_subscribe')</span></button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
