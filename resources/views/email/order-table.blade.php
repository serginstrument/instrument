<h2>Order from "Instruments shop"</h2>

<p>Thank you for placing an order in our store. Please pay for the order within 2 days,</p>
<p>otherwise the goods will be removed from the reserve.</p>
<br>
<p>Account for payment 2222 3333 4444 5555</p>
<br>
<table>
    {!! $table !!}
</table>
<br>
<p>Sincerely, the staff of the store "Instruments shop"!</p>
