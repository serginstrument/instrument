@extends('layouts.main')

@section('title', 'Registration')
@section('custom_css')
    <link rel="stylesheet" type="text/css" href="/styles/product.css">
    <link rel="stylesheet" type="text/css" href="/styles/product_responsive.css">
@endsection

@section('content')
    <div class="home">

      <section class="content">
         <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                     <h3 class="m-0">@lang('login.registration')</h3>
                     <div class="card card-primary">
                        <!-- form start -->
                        <form action="{{route('user.registration')}}" method="POST">
                            @csrf
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="first_name">@lang('login.first_name')</label>
                                    <input type="text"
                                           name="first_name"
                                           value="" id="first_name"
                                           class="form-control"
                                           placeholder="@lang('login.enter_first_name')">
                                    @error('first_name')
                                    <div class="alert alert-danger"> {{$message}}</div>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label for="last_name">@lang('login.last_name')</label>
                                    <input class="form-control" type="text"
                                           name="last_name"
                                           value="" id="last_name" placeholder="@lang('login.enter_last_name')">
                                    @error('last_name')
                                    <div class="alert alert-danger"> {{$message}}</div>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label for="email">@lang('login.email')</label>
                                    <input class="form-control" type="text"
                                           name="email"
                                           value="{{old('email')}}" id="email"
                                           placeholder="@lang('login.enter_email')">
                                    @error('email')
                                    <div class="alert alert-danger"> {{$message}}</div>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label for="password">@lang('login.password')</label>
                                    <input type="password"
                                           name="password"
                                           value=""
                                           class="form-control"
                                           id="password"
                                           placeholder="@lang('login.enter_password')">
                                    @error('password')
                                    <div class="alert alert-danger"> {{$message}}</div>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label for="password">@lang('login.repeat_password')</label>
                                    <input type="password"
                                           name="password_confirmation"
                                           value=""
                                           class="form-control"
                                           id="password"
                                           placeholder="@lang('login.confirm_password')">
                                    @error('password')
                                    <div class="alert alert-danger"> {{$message}}</div>
                                    @enderror
                                </div>

                            </br>
                            <button type="submit" class="btn btn-success">@lang('login.sign_up')</button>
                            <a class="me-3 py-2 text-dark text-decoration-none" href="{{route ('user.login')}}">
                                @lang('login.sign_in')
                            </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
         </div><!-- /.container-fluid -->
      </section>
      </br>
    </div>

@endsection
