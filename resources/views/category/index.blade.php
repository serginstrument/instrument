@extends('layouts.main')

@section('title', 'Categories')

@section('content')
    <br>
    <div class="products">
        <div class="container" style="margin: auto;">
            <div class="row">
                @foreach ($categories as $index => $category)
                    <div class="col-md-4">
                        <div class="card my-4 category-card">
                            <img src="{{ asset('storage/' . $category->image) }}" width="200"
                                 height="250"
                                 alt="" class="card-img-top">
                            <div class="card-body">
                                <div class="card-title">
                                    <h3 style="color:darkslategray">{{$category->__('name')}}</h3>
                                </div>
                                <p> {{$category->__('description')}}</p>
                                <a href="{{route ('category.show', $category)}}" class="btn btn-outline-success">@lang('category.button_go_to_category')</a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

@endsection

