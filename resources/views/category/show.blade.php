@extends('layouts.main')

@section('title', $category->name)

@section('custom_css')
    <link rel="stylesheet" type="text/css" href="/styles/categories.css">
    <link rel="stylesheet" type="text/css" href="/styles/categories_responsive.css">
@endsection

@section('content')

    <div class="home">
        <div class="home_container">
            <div class="home_background" style="background-image:url({{ asset('storage/' . $category->image) }})"></div>
            <div class="home_content_container">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <div class="home_content">
                                <div class="home_title">{{$category->__('name')}}<span>.</span></div>
                                <div class="home_text">
                                    <p>{{$category->__('description')}}</p>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Products -->

    <div class="products">
        <style>
            .product:hover {
                box-shadow: 0 0 2px rgba(0, 0, 0, 0.3);
                border: 1px solid green;
            }
        </style>
        <div class="container">
            <div class="row">
                <div class="col">

                    <!-- Product Sorting -->
                    <div class="sorting_bar d-flex flex-md-row flex-column align-items-md-center justify-content-md-start">
                        <div class="results">@lang('category.showing') <span>{{$category->instruments->count()}}</span> @lang('category.results')</div>
                        <div class="sorting_container ml-md-auto">
                            <div class="sorting">
                                <ul class="item_sorting">
                                    <li>
                                        <span class="sorting_text">@lang('category.sort')</span>
                                        <i class="fa fa-chevron-down" aria-hidden="true"></i>
                                        <ul>
                                            <li class="product_sorting_btn" data-order="default"><span>Default</span></li>
                                            <li class="product_sorting_btn" data-order="price-low-high"><span>Price: Low-High</span></li>
                                            <li class="product_sorting_btn" data-order="price-high-low"><span>Price: High-Low</span></li>
                                            <li class="product_sorting_btn" data-order="name-a-z"><span>Name: A-Z</span></li>
                                            <li class="product_sorting_btn" data-order="name-z-a"><span>Name: Z-A</span></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">

                    <div class="product_grid">
                        @foreach($category->instruments as $instrument)
                            @php
                                $image = '';
                                if (count($instrument->images) > 0){
                                    $image = $instrument->images[0]['img'];
                                } else {
                                $image = '/images/no_image.jpg';
                                }
                            @endphp
                            <div class="product" style="
                             padding: 20px ;  background-color: #F8F8FF;">
                                <div class="product_image"><a href="{{route('instrument.show', $instrument)}}"><img src="{{ count($instrument->images) > 0 ? '/storage/'.$image : '/images/no_image.jpg' }}" width="200" height="250" alt="" class="card-img-top">
                                </div>
                                <div class="product_extra product_new"><a href="#" style=" background-color: #DEB887" >{{$instrument['manufacture']['name']}}</a></div>
                                <div class="product_content">
                                    <div class="product_title" style=" white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
                                        <a href="{{route('instrument.show', $instrument)}}" style="font-weight:lighter; font-size:1.2em; color:darkslategray" >{{ $instrument->__('name') }}</a>
                                    </div>
                                    @if($instrument->new_price != null)
                                        <div class="details_discount" >{{ number_format($instrument->price, 2, '.', ' ') }}UAH</div>
                                        <div class="details_price" style="font-weight:inherit;  font-size: 20px; color:#3CB371">{{ number_format($instrument->new_price, 2, '.', ' ') }}UAH</div>
                                    @else
                                        <div class="details_price" style="font-weight:inherit;  font-size: 20px; color:#3CB371">{{ number_format($instrument->price, 2, '.', ' ') }}UAH</div>
                                    @endif
                                </div>
                                @if($instrument->leftovers > 0)
                                    <span style="color: green">@lang('category.in_stock')</span>
                                @else
                                    <span>@lang('category.unavailable')</span>
                                @endif
                            </div>
                        @endforeach
                    </div>
                    {{$instruments->links()}}
{{--                                        {{$instruments->appends(request()->query())->links('pagination.index')}}--}}
                </div>
            </div>
        </div>
    </div>


    <!-- Icon Boxes -->

    <div class="icon_boxes">
        <div class="container">
            <div class="row icon_box_row">

                <!-- Icon Box -->
                <div class="col-lg-4 icon_box_col">
                    <div class="icon_box">
                        <div class="icon_box_image"><img src="/images/icon_1.svg" alt=""></div>
                        <div class="icon_box_title">@lang('category.free_shipping_worldwide')</div>
                        <div class="icon_box_text">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam a ultricies metus. Sed nec molestie.</p>
                        </div>
                    </div>
                </div>

                <!-- Icon Box -->
                <div class="col-lg-4 icon_box_col">
                    <div class="icon_box">
                        <div class="icon_box_image"><img src="/images/icon_2.svg" alt=""></div>
                        <div class="icon_box_title">@lang('category.free_returns')</div>
                        <div class="icon_box_text">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam a ultricies metus. Sed nec molestie.</p>
                        </div>
                    </div>
                </div>

                <!-- Icon Box -->
                <div class="col-lg-4 icon_box_col">
                    <div class="icon_box">
                        <div class="icon_box_image"><img src="/images/icon_3.svg" alt=""></div>
                        <div class="icon_box_title">@lang('category.fast_support')</div>
                        <div class="icon_box_text">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam a ultricies metus. Sed nec molestie.</p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <!-- Newsletter -->

    <div class="newsletter">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="newsletter_border"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <div class="newsletter_content text-center">
                        <div class="newsletter_title">@lang('category.subscribe')</div>
                        <div class="newsletter_text"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam a ultricies metus. Sed nec molestie eros</p></div>
                        <div class="newsletter_form_container">
                            <form action="#" id="newsletter_form" class="newsletter_form">
                                <input type="email" class="newsletter_input" required="required">
                                <button class="newsletter_button trans_200"><span>@lang('category.button_subscribe')</span></button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom_js')
    <script>
        $(document).ready(function () {
            $('.product_sorting_btn').click(function () {
                let orderBy = $(this).data('order')
                $('.sorting_text').text($(this).find('span').text())
                $.ajax({
                    url: "{{route('category.show', $category)}}",
                    type: "GET",
                    data: {
                        orderBy: orderBy,
                        page: {{isset($_GET['page']) ? $_GET['page'] : 1}},
                    },
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: (data) => {
                        let positionParameters = location.pathname.indexOf('?');
                        let url = location.pathname.substring(positionParameters,location.pathname.length);
                        let newURL = url + '?'; // http://127.0.0.1:8001/phones?
                        newURL += "&page={{isset($_GET['page']) ? $_GET['page'] : 1}}"+'orderBy=' + orderBy; // http://127.0.0.1:8001/phones?orderBy=name-z-a
                        history.pushState({}, '', newURL);
                        $('.product_pagination a').each(function(index, value){
                            let link= $(this).attr('href')
                            $(this).attr('href',link+'&orderBy='+orderBy)
                        })
                        $('.product_grid').html(data)
                        $('.product_grid').isotope('destroy')
                        $('.product_grid').imagesLoaded( function() {
                            let grid = $('.product_grid').isotope({
                                itemSelector: '.instrument',
                                layoutMode: 'fitRows',
                                fitRows:
                                    {
                                        gutter: 30
                                    }
                            });
                        });
                    }
                });
            })
        })
    </script>
@endsection
