@extends('admin.admin_main')

@section('title-block')
    Permission editing form
@endsection

@section('content')

    <form action="{{route('admin.permission.update', $permission->getKey())}}" method="POST">
        @csrf
        @method('PUT')
        <div class="card-body">
            <h1>Permission editing form</h1>
            <br>
            <div class="col-lg-12">
                <div class="form-group" style=" width: 50%;">
                    <label for="name">Permission name</label>
                    <input type="text"
                           name="name"
                           value="{{old ('name',$permission->name)}}" id="name"
                           class="form-control"
                           placeholder="">
                    @error('name')
                    <div class="alert alert-danger"> {{$message}}</div>
                    @enderror
                </div>
            </div>

            <div class="col-lg-12">
                <div class="form-group" style=" width: 50%;">
                    <label for="name">Permission slug</label>
                    <input type="text"
                           name="slug"
                           value="{{old ('name',$permission->slug)}}" id="name"
                           class="form-control"
                           placeholder="">
                    @error('slug')
                    <div class="alert alert-danger"> {{$message}}</div>
                    @enderror
                </div>
            </div>
            <br>
            <button type="submit" style=" width: 120px;" class="btn btn-success">{{__('Save')}}</button>
        </div>
    </form>
@endsection

