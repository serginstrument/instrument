@extends('admin.admin_main')

@section('title-block')
    Permissions
@endsection

@section('content')
    <div class="col-lg-12">
        <table class="table table-striped">
            <label>
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Actions</th>
                </tr>
                </thead>

                <tbody>
                <div class="card-footer">
                    <h1>Permissions</h1>
                    <br>
                    <a class="btn btn-outline-success my-2 my-sm-0"
                       href="{{route('admin.permission.create')}}" style="width: 120px">Create</a>

                    @foreach ($permissions as $permission)
                        <tr>
                            <th scope="row">{{$loop->iteration + $permissions->perPage()*($permissions->currentPage()-1)}}</th>
                            <td>
                                <p>{{ucfirst($permission->slug)}}</p>
                            </td>

                            <td class="project-actions text-middle">
                                <form action="{{route ('admin.permission.destroy', $permission)}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <a class="btn btn-info btn-sm wider-btn" href="{{route ('admin.permission.edit', $permission)}}">
                                        <i class="fas fa-pencil-alt"></i>
                                        {{__('Edit')}}
                                    </a>

                                    <style>
                                        .wider-btn {
                                            width: 120px;
                                        }
                                    </style>
                                    <button type="submit" class="btn btn-danger btn-sm">
                                        <i class="fas fa-trash"></i>
                                        {{__('Delete')}}
                                    </button>
                                </form>
                            </td>
                        </tr>
                @endforeach
                </tbody>
            </label>
        </table>
        {{$permissions->links()}}
    </div>
@endsection
