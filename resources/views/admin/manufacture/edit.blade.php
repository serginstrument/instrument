@extends('admin.admin_main')

@section('title-block')
    Manufacture editing form
@endsection

@section('content')

    <form action="{{route('admin.manufacture.update', $manufacture->getKey())}}" method="POST">
        @csrf
        @method('PUT')
        <div class="card-body">
            <h1>Manufacture editing form</h1>
            <br>
            <div class="col-lg-12">
                <div class="form-group" style=" width: 50%;">
                    <label for="name">Manufacture name</label>
                    <input type="text"
                           name="name"
                           value="{{old ('name',$manufacture->name)}}" id="name"
                           class="form-control"
                           placeholder="">
                    @error('name')
                    <div class="alert alert-danger"> {{$message}}</div>
                    @enderror
                </div>
            </div>
            </br>
            <button type="submit" style=" width: 120px;" class="btn btn-success">{{__('Save')}}</button>
        </div>
    </form>
@endsection

