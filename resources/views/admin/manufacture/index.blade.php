@extends('admin.admin_main')

@section('title-block')
    Manufactures
@endsection

@section('content')
    <div class="col-lg-12">
        <table class="table table-striped">
            <label>
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Actions</th>
                </tr>
                </thead>
                <tbody>
                <div class="card-footer">
                    <h1>Manufactures</h1>
                    <br>
                    <a class="btn btn-outline-success my-2 my-sm-0" style="width: 120px"
                       href="{{route('admin.manufacture.create')}}">Create</a>

                    @if(session('success'))
                        <div class="alert alert-success mb-0 rounded-0 text-center small py-2">
                            {{ session('success') }}
                        </div>
                    @endif

                    @if (session('error'))
                        <div class="alert alert-danger mb-0 rounded-0 text-center small py-2">
                            {{ session('error') }}
                        </div>
                    @endif

                    @foreach ($manufactures as $manufacture)
                        <tr>
                            <th scope="row">{{$loop->iteration + $manufactures->perPage()*($manufactures->currentPage()-1)}}</th>
                            <td>{{ucfirst($manufacture->name)}}</td>
                            <td class="project-actions text-middle">
                                <form action="{{route ('admin.manufacture.destroy', $manufacture)}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <a class="btn btn-info btn-sm wider-btn" href="{{route ('admin.manufacture.edit', $manufacture)}}">
                                        <i class="fas fa-pencil-alt"></i>
                                        {{__('Edit')}}
                                    </a>

                                    <style>
                                        .wider-btn {
                                            width: 120px;
                                        }
                                    </style>
                                    <button type="submit" class="btn btn-danger btn-sm">
                                        <i class="fas fa-trash"></i>
                                        {{__('Delete')}}
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </div>
                </tbody>
            </label>
        </table>
    </div>
@endsection
