@extends('admin.admin_main')

@section('title-block')
    Manufacture creating form
@endsection

@section('content')

    <form action="{{route('admin.manufacture.store')}}" method="POST">
        @csrf
        <div class="card-body">
            <h1>Manufacture creating form</h1>
            <br>
            <div class="form-group" style=" width: 50%;">
                <label for="category">Manufacture name</label>
                <input type="text"
                       name="name"
                       value="{{old ('name')}}" id="name"
                       class="form-control"
                       placeholder="">
                @error('name')
                <div class="alert alert-danger"> {{$message}}</div>
                @enderror
            </div>
            <br>
            <button type="submit" style=" width: 120px;" class="btn btn-success">{{__('Save')}}</button>
        </div>
    </form>

@endsection


