@extends('admin.admin_main')

@section('title-block')
    Category editing form
@endsection

@section('content')
    <div class="col-lg-12">
        <form action="{{route('admin.category.update', $category->getKey())}}" method="POST"
              enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="card-body">
                <h1>Category editing form</h1>
                <br>
                <div class="col-lg-12">
                    <div class="form-group" style=" width: 50%; margin-bottom: 40px;">
                        <label for="name">Category name</label>
                        <input type="text"
                               name="name"
                               value="{{old ('name',$category->name)}}" id="name"
                               class="form-control"
                               placeholder="">
                        @error('name')
                        <div class="alert alert-danger"> {{$message}}</div>
                        @enderror
                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="form-group" style=" width: 50%; margin-bottom: 40px;">
                        <label for="name_ukr">Category name UKR</label>
                        <input type="text"
                               name="name_ukr"
                               value="{{old ('name_ukr',$category->name_ukr)}}" id="name_ukr"
                               class="form-control"
                               placeholder="">
                        @error('name_ukr')
                        <div class="alert alert-danger"> {{$message}}</div>
                        @enderror
                    </div>
                </div>

                <div class="form-group" style=" width: 50%; margin-bottom: 40px;">
                    <label for="image">Image</label>
                    <input type="file" name="image" class="form-control" id="image">
                    @error('image')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="form-group" style=" width: 50%;">
                    <label for="description">Description</label>
                    <textarea name="description"
                              class="form-control"
                              id="description"
                              placeholder="">{{old ('description', $category->description)}}</textarea>
                    @error('description')
                    <div class="alert alert-danger"> {{$message}}</div>
                    @enderror
                </div>

                <div class="form-group" style=" width: 50%;">
                    <label for="description_ukr">Description UKR</label>
                    <textarea name="description_ukr"
                              class="form-control"
                              id="description_ukr"
                              placeholder="">{{old ('description_ukr', $category->description_ukr)}}</textarea>
                    @error('description_ukr')
                    <div class="alert alert-danger"> {{$message}}</div>
                    @enderror
                </div>

                <br>

                <button type="submit" style=" width: 120px;" class="btn btn-success">{{__('Save')}}</button>
            </div>
        </form>
    </div>
@endsection

