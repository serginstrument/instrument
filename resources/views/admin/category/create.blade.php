@extends('admin.admin_main')

@section('title-block')
    Category creating form
@endsection

@section('content')
    <div class="col-lg-12">
        <form action="{{route('admin.category.store')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="card-body">
                <h1>Category creating form</h1>
                <br>
                <div class="form-group" style=" width: 50%; margin-bottom: 40px;">
                    <label for="name">Category name</label>
                    <input type="text"
                           name="name"
                           value="{{old ('name')}}" id="name"
                           class="form-control"
                           placeholder="category name">
                    @error('name')
                    <div class="alert alert-danger"> {{$message}}</div>
                    @enderror
                </div>

                <div class="form-group" style=" width: 50%; margin-bottom: 40px;">
                    <label for="name_ukr">Category name UKR</label>
                    <input type="text"
                           name="name_ukr"
                           value="{{old ('name_ukr')}}" id="name_ukr"
                           class="form-control"
                           placeholder="category name">
                    @error('name_ukr')
                    <div class="alert alert-danger"> {{$message}}</div>
                    @enderror
                </div>

                <div class="form-group" style=" width: 50%; margin-bottom: 40px;">
                    <label for="image">Image</label>
                    <input type="file" name="image" class="form-control" id="image">
                    @error('image')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="form-group" style=" width: 50%;">
                    <label for="description">Description</label>
                    <textarea name="description"
                              class="form-control"
                              id="description"
                              placeholder="">{{old ('description')}}</textarea>
                    @error('description')
                    <div class="alert alert-danger"> {{$message}}</div>
                    @enderror
                </div>

                <div class="form-group" style=" width: 50%;">
                    <label for="description_ukr">Description UKR</label>
                    <textarea name="description_ukr"
                              class="form-control"
                              id="description_ukr"
                              placeholder="">{{old ('description_ukr')}}</textarea>
                    @error('description_ukr')
                    <div class="alert alert-danger"> {{$message}}</div>
                    @enderror
                </div>

                <br>
                <button type="submit" style=" width: 120px;" class="btn btn-success">{{__('Save')}}</button>
            </div>
        </form>
    </div>
@endsection


