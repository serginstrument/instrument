@extends('admin.admin_main')

@section('title-block')
    User editing form
@endsection

@section('content')

    <form action="{{route('admin.user.update', $user->getKey())}}" method="POST">
        @csrf
        @method('PUT')
        <div class="card-body">

            <h1>User editing form</h1>
            <br>
            <div class="form-group">
                <label for="first_name">First name</label>
                <input type="text"
                       name="first_name"
                       value="{{old ('first_name', $user->first_name)}}" id="first_name"
                       class="form-control"
                       placeholder="">
                @error('first_name')
                <div class="alert alert-danger"> {{$message}}</div>
                @enderror
            </div>

            <div class="form-group">
                <label for="last_name">Last name</label>
                <input type="text"
                       name="last_name"
                       value="{{old ('last_name', $user->last_name)}}" id="last_name"
                       class="form-control"
                       placeholder="">
                @error('last_name')
                <div class="alert alert-danger"> {{$message}}</div>
                @enderror
            </div>

            <div class="form-group">
                <div class="form-group">
                    <label>Role</label>
                    <select name="role_id" class="form-control">
                        @foreach($roles as $role)
                            <option value="{{$role['id']}}"
                                    @if (isset($user) && $user->roles->contains($role)) selected @endif>
                                {{$role['slug']}}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label for="phone">Phone</label>
                <input class="form-control" type="text"
                       name="phone"
                       value="{{old('phone', $user->phone)}}" id="phone"
                       placeholder="">
                @error('phone')
                <div class="alert alert-danger">{{$message}}</div>
                @enderror
            </div>

            <div class="form-group">
                <label for="email">email</label>
                <input type="text"
                       name="email"
                       value="{{old ('email', $user->email)}}"
                       class="form-control"
                       id="email"
                       placeholder="">
                @error('email')
                <div class="alert alert-danger"> {{$message}}</div>
                @enderror
            </div>

            </br>
            <button type="submit" class="btn btn-success">{{__('Save')}}</button>
        </div>
    </form>
@endsection

