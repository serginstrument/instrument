@extends('admin.admin_main')

@section('title-block')
    User creating form
@endsection

@section('content')

    <form action="{{route('admin.user.store')}}" method="POST">
        @csrf
        <div class="card-body">
            <h1>User creating form</h1>
            <br>
            <div class="form-group">
                <label for="first_name">First name</label>
                <input type="text"
                       name="first_name"
                       value="{{old ('first_name')}}" id="first_name"
                       class="form-control"
                       placeholder="first name">
                @error('first_name')
                <div class="alert alert-danger"> {{$message}}</div>
                @enderror
            </div>

            <div class="form-group">
                <label for="last_name">Last name</label>
                <input type="text"
                       name="last_name"
                       value="{{old ('last_name')}}" id="last_name"
                       class="form-control"
                       placeholder="last name">
                @error('last_name')
                <div class="alert alert-danger"> {{$message}}</div>
                @enderror
            </div>

            <div class="form-group">
                <div class="form-group">
                    <label for="role">Role</label>
                    <select name="role_id" class="form-control">
                        @foreach($roles as $role)
                            <option value="{{$role['id']}}">{{$role['slug']}}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label for="phone">Phone</label>
                <input class="form-control" type="text"
                       name="phone"
                       value="{{old('phone')}}" id="phone"
                       placeholder="phone">
                @error('phone')
                <div class="alert alert-danger"> {{$message}}</div>
                @enderror
            </div>

            <div class="form-group">
                <label for="email">email</label>
                <input type="text"
                       name="email"
                       value="{{old ('email')}}"
                       class="form-control"
                       id="email"
                       placeholder="email">
                @error('email')
                <div class="alert alert-danger"> {{$message}}</div>
                @enderror
            </div>

            <div class="form-group">
                <label for="password">Password</label>
                <input type="text"
                       name="password"
                       value="{{old ('password')}}"
                       class="form-control"
                       id="password"
                       placeholder="password">
                @error('password')
                <div class="alert alert-danger"> {{$message}}</div>
                @enderror
            </div>

            <div class="form-group">
                <label for="password">Repeat password</label>
                <input type="password"
                       name="password_confirmation"
                       value=""
                       class="form-control"
                       id="password"
                       placeholder="enter password">
                @error('password')
                <div class="alert alert-danger"> {{$message}}</div>
                @enderror
            </div>

            <br>
            <button type="submit" class="btn btn-success">{{__('Save')}}</button>
        </div>
    </form>
@endsection


