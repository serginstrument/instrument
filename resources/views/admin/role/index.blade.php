@extends('admin.admin_main')

@section('title-block')
    Roles
@endsection

@section('content')
    <div class="col-lg-12">
        <table class="table table-striped">
            <label>
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Actions</th>
                </tr>
                </thead>

                <tbody>
                <div class="card-footer">
                    <h1>Roles</h1>
                    <br>
                    <a class="btn btn-outline-success my-2 my-sm-0" style="width: 120px" href="{{route('admin.role.create')}}">Create</a>
                    @foreach ($roles as $role)
                        <tr>
                            <th scope="row">{{$loop->iteration + $roles->perPage()*($roles->currentPage()-1)}}</th>
                            <td>
                                <p>{{ucfirst($role->slug)}}</p>
                            </td>
                            <td class="project-actions text-middle">
                                <form action="{{route ('admin.role.destroy', $role)}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <a class="btn btn-info btn-sm wider-btn" href="{{route ('admin.role.edit', $role)}}">
                                        <i class="fas fa-pencil-alt"></i>
                                        {{__('Edit')}}
                                    </a>

                                    <style>
                                        .wider-btn {
                                            width: 120px;
                                        }
                                    </style>
                                    <button type="submit" class="btn btn-danger btn-sm">
                                        <i class="fas fa-trash"></i>
                                        {{__('Delete')}}
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </div>
                </tbody>
            </label>
        </table>
    </div>

@endsection
