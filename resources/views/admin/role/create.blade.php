@extends('admin.admin_main')

@section('title-block')
    Role creating form
@endsection

@section('content')

    <form action="{{route('admin.role.store')}}" method="POST">
        @csrf
        <div class="card-body">
            <h1>Role creating form</h1>
            <br>
            <div class="form-group" style=" width: 50%;">
                <label for="name">Role name</label>
                <input type="text"
                       name="name"
                       value="{{old ('name')}}" id="name"
                       class="form-control"
                       placeholder="role name">
                @error('name')
                <div class="alert alert-danger"> {{$message}}</div>
                @enderror
            </div>

            <div class="form-group" style=" width: 50%;">
                <label for="role">Role slug</label>
                <input type="text"
                       name="slug"
                       value="{{old ('slug')}}" id="slug"
                       class="form-control"
                       placeholder="role slug">
                @error('slug')
                <div class="alert alert-danger"> {{$message}}</div>
                @enderror
            </div>
            <br>
            <div class="form-group" style=" width: 50%;">
                <label for="permission">Сhoose permissions</label>
                <select multiple="multiple" name="permissions[]" class="form-control">
                    @foreach($permissions as $permission)
                        <option value="{{$permission['id']}}">{{$permission['slug']}}</option>
                    @endforeach
                </select>
            </div>

            <br>
            <button type="submit" style=" width: 120px;" class="btn btn-success">{{__('Save')}}</button>
        </div>
    </form>

@endsection


