@extends('admin.admin_main')

@section('title-block')
    Role editing form
@endsection

@section('content')

    <form action="{{route('admin.role.update', $role->getKey())}}" method="POST">
        @csrf
        @method('PUT')

        <div class="card-body">
            <h1>Role editing form</h1>
            <br>
            <div class="col-lg-12">
                <div class="form-group" style=" width: 50%;">
                    <label for="name">Role name</label>
                    <input type="text"
                           name="name"
                           value="{{old ('name',$role->name)}}" id="name"
                           class="form-control">
                    @error('name')
                    <div class="alert alert-danger"> {{$message}}</div>
                    @enderror
                </div>
            </div>

            <div class="col-lg-12">
                <div class="form-group" style=" width: 50%;">
                    <label for="slug">Role slug</label>
                    <input type="text"
                           name="slug"
                           value="{{old ('slug',$role->slug)}}" id="slug"
                           class="form-control">
                    @error('slug')
                    <div class="alert alert-danger"> {{$message}}</div>
                    @enderror
                </div>
            </div>

            <div class="form-group" style=" width: 50%;">
                <label>Сhoose permissions</label>
                <select multiple="multiple" name="permissions[]" class="form-control">
                    @foreach($permissions as $permission)
                        <option value="{{$permission['id']}}"
                                @if (isset($role) && $role->permissions->contains($permission)) selected @endif>
                            {{$permission['slug']}}
                        </option>
                    @endforeach
                </select>
            </div>

            <br>
            <button type="submit" style=" width: 120px;" class="btn btn-success">{{__('Save')}}</button>
        </div>
    </form>
@endsection

