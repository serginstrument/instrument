@extends('admin.admin_main')

@section('title-block')
    Instrument editing form
@endsection

@section('content')
    <div class="col-lg-12">
        <form action="{{route('admin.instrument.update', $instrument->getKey())}}" method="POST"
              enctype="multipart/form-data">
            @csrf
            @method('PUT')

            <div class="card-body">
                <h1>Instrument editing form</h1>
                <br>
                <div class="form-group" style="display: inline-block; width: 30%; margin-bottom: 60px;">
                    <label for="name">Name</label>
                    <input type="text"
                           name="name"
                           value="{{old ('name',$instrument->name)}}" id="name"
                           class="form-control"
                           placeholder="">
                    @error('name')
                    <div class="alert alert-danger"> {{$message}}</div>
                    @enderror
                </div>

                <div class="form-group" style="display: inline-block; width: 30%; margin-bottom: 60px;">
                    <label for="name_ukr">Name UKR</label>
                    <input type="text"
                           name="name_ukr"
                           value="{{old ('name_ukr',$instrument->name_ukr)}}" id="name_ukr"
                           class="form-control"
                           placeholder="">
                    @error('name_ukr')
                    <div class="alert alert-danger"> {{$message}}</div>
                    @enderror
                </div>

                <div class="form-group">
                    <div class="form-group" style="display: inline-block; width: 15%; margin-right: 2rem;">
                        <label for="price">Price</label>
                        <div class="input-group">
                            <input class="form-control" type="text" name="price"
                                   value="{{ old('price', $instrument->price) }}" id="price"
                                   placeholder="price">
                            <div class="input-group-append">
                                <span class="input-group-text">Uah</span>
                            </div>
                        </div>
                        @error('price')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>

                    <div class="form-group" style="display: inline-block; width: 15%; margin-bottom: 20px;">
                        <label for="new_price">New price</label>
                        <div class="input-group">
                            <input class="form-control" type="text" name="new_price"
                                   value="{{ old('new_price', $instrument->new_price) }}" id="new_price"
                                   placeholder="new price">
                            <div class="input-group-append">
                                <span class="input-group-text">Uah</span>
                            </div>
                        </div>
                        @error('new_price')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>

                <div class="form-group"
                     style="display: inline-block; width: 15%; margin-right: 2rem; margin-bottom: 60px;">
                    <label for="leftovers">Leftovers</label>
                    <div class="input-group">
                        <input class="form-control" type="text" name="leftovers"
                               value="{{ old('leftovers', $instrument->leftovers) }}" id="leftovers"
                               placeholder="leftovers">
                        <div class="input-group-append">
                            <span class="input-group-text">pcs</span>
                        </div>
                    </div>
                    @error('leftovers')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="form-group">
                    <div class="form-group" style="display: inline-block; width: 20%;margin-right: 2rem;">
                        <label>Manufacture</label>
                        <select name="manufacture_id" class="form-control">
                            @foreach($manufactures as $manufacture)
                                <option value="{{$manufacture->getKey()}}"
                                        @if ($manufacture['id'] == $instrument['manufacture_id']) selected @endif>
                                    {{$manufacture['name']}}</option>
                            @endforeach
                        </select>

                    </div>
                    <div class="col-lg-12" style="display: inline-block; width: 20%; margin-bottom: 60px;">
                        <div class="form-group">
                            <label>Category</label>
                            <select name="category_id" class="form-control">
                                @foreach($categories as $category)
                                    <option value="{{$category['id']}}"
                                            @if (isset($instrument) && $instrument->categories->contains($category)) selected @endif>
                                        {{$category['name']}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                @if (count($instrument->images) > 0)
                    <div class="form-group">
                        <label>Current images:</label>
                        <div class="row">
                            @foreach ($instrument->images as $image)
                                <div class="col-md-3">
                                    <img src="{{ asset('storage/' . $image->img) }}" width="200"
                                         height="200" class="img-thumbnail"
                                         alt="{{ $image->img }}">
                                    <div class="form-check">
                                        <input type="checkbox" name="delete_image" value="{{ $image->id }}"
                                               class="form-check-input">
                                        <label class="form-check-label">Delete</label>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                @endif

                <label>Add images:</label>
                <input type="file" name="images[]" class="form-control-file" multiple>

                <div class="form-group" style=" width: 50%;">
                    <label for="description">Description</label>
                    <textarea name="description"
                              class="form-control"
                              id="description"
                              placeholder="">{{old ('description', $instrument->description)}}</textarea>
                    @error('description')
                    <div class="alert alert-danger"> {{$message}}</div>
                    @enderror
                </div>

                <div class="form-group" style=" width: 50%;">
                    <label for="description_ukr">Description UKR</label>
                    <textarea name="description_ukr"
                              class="form-control"
                              id="description_ukr"
                              placeholder="">{{old ('description_ukr', $instrument->description_ukr)}}</textarea>
                    @error('description_ukr')
                    <div class="alert alert-danger"> {{$message}}</div>
                    @enderror
                </div>

                <br>
                <button type="submit" style="width: 120px" class="btn btn-success">{{__('Save')}}</button>


            </div>

        </form>
    </div>

@endsection

