@extends('admin.admin_main')

@section('title-block')
    Instruments
@endsection

@section('content')
    <div class="col-lg-12">
        <table class="table table-striped">
            <label>
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Actions</th>
                </tr>
                </thead>

                <tbody>
                <h1>Instruments</h1>
                <br>
                <div class="d-flex justify-content-end">

                    <a class="btn btn-outline-success mr-2" style="width: 120px"
                       href="{{route('admin.instrument.create')}}">Create</a>

                    <div class="container">
                        <form action="{{route('admin.instrument.index')}}" method="get"
                              class="d-flex align-items-center justify-content-end w-auto">
                            <div class="input-group">
                                <input name="search_name"
                                       @if(isset($_GET['search_name'])) value="{{$_GET['search_name']}}"
                                       @endif type="text" class="form-control" placeholder="Search instrument"
                                       aria-label="Search">
                            </div>
                        </form>
                    </div>

                    <form action="{{ route('admin.import-instruments') }}" method="post" enctype="multipart/form-data">
                        @csrf

                        <div class="input-group mb-3" style="width: 500px;  margin-right: 0.5rem;">
                            <input type="file" class="form-control" id="instruments" name="instruments">

                        </div>

                        <button type="submit" class="btn btn-block btn-outline-primary"
                                style="width: 150px; margin-right: 0.5rem;">Import
                        </button>
                    </form>
                    <a class="btn btn-outline-warning" style="width: 150px; margin-right: 0.5rem;"
                       href="{{ route('admin.export-instruments') }}">Export</a>
                </div>

                @if(session('success'))
                    <div class="alert alert-success mb-0 rounded-0 text-center small py-2">
                        {{ session('success') }}
                    </div>
                @endif

                @if (session('error'))
                    <div class="alert alert-danger mb-0 rounded-0 text-center small py-2">
                        {{ session('error') }}
                    </div>
                @endif
                <br>
                <div class="results">Showing <span>{{$instruments->total()}}</span> results</div>

                @foreach ($instruments as $instrument)
                    <tr>
                        <th scope="row">{{$loop->iteration + $instruments->perPage()*($instruments->currentPage()-1)}}</th>
                        <td>
                            <p>{{$instrument->name}}</p>
                        </td>

                        <td class="project-actions text-middle">
                            <form
                                action="{{ route('admin.instrument.destroy', ['instrument' => $instrument, 'page' => $instruments->currentPage()])}}"
                                method="POST">
                                @csrf
                                @method('DELETE')
                                <a class="btn btn-info btn-sm wider-btn"
                                   href="{{route ('admin.instrument.edit', $instrument)}}">
                                    <i class="fas fa-pencil-alt"></i>
                                    {{__('Show & edit')}}
                                </a>

                                <style>
                                    .wider-btn {
                                        width: 120px;
                                    }
                                </style>
                                <button type="submit" class="btn btn-danger btn-sm">
                                    <i class="fas fa-trash"></i>
                                    {{__('Delete')}}
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </label>
        </table>
        {{$instruments->withQueryString()->links('vendor.pagination.bootstrap-4')}}
    </div>
@endsection
