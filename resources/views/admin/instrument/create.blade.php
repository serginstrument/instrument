@extends('admin.admin_main')

@section('title-block')
    Instrument creating form
@endsection

@section('content')
    <div class="col-lg-12">
        <form action="{{route('admin.instrument.store')}}" method="POST" enctype="multipart/form-data">
            @csrf

            <div class="card-body">
                <h1>Instrument creating form</h1>
                 <br>
                <div class="form-group" style=" width: 30%; margin-bottom: 60px;">
                    <label for="name">Name</label>
                    <input type="text"
                           name="name"
                           value="{{old ('name')}}" id="name"
                           class="form-control"
                           placeholder="name">
                    @error('name')
                    <div class="alert alert-danger"> {{$message}}</div>
                    @enderror
                </div>

                <div class="form-group" style=" width: 30%; margin-bottom: 60px;">
                    <label for="name_ukr">Name UKR</label>
                    <input type="text"
                           name="name_ukr"
                           value="{{old ('name_ukr')}}" id="name_ukr"
                           class="form-control"
                           placeholder="name_ukr">
                    @error('name_ukr')
                    <div class="alert alert-danger"> {{$message}}</div>
                    @enderror
                </div>

                <div class="form-group">
                    <div class="form-group" style="display: inline-block; width: 15%; margin-right: 2rem;">
                        <label for="price">Price</label>
                        <div class="input-group">
                            <input class="form-control" type="text" name="price"
                                   value="{{ old('price', $instrument->price) }}" id="price" placeholder="price">
                            <div class="input-group-append">

                                <span class="input-group-text">Uah</span>
                            </div>
                        </div>
                        @error('price')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>

                    <div class="form-group" style="display: inline-block; width: 15%;">
                        <label for="new_price">New price</label>
                        <div class="input-group">
                            <input class="form-control" type="text" name="new_price"
                                   value="{{ old('new_price', $instrument->new_price) }}" id="new_price"
                                   placeholder="new price">
                            <div class="input-group-append">
                                <span class="input-group-text">Uah</span>
                            </div>
                        </div>
                        @error('new_price')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>

                <div class="form-group" style="display: inline-block; width: 15%;  margin-bottom: 60px;">
                    <label for="leftovers">Leftovers</label>
                    <div class="input-group">
                        <input class="form-control" type="text" name="leftovers"
                               value="{{ old('price', $instrument->leftovers) }}" id="price" placeholder="leftovers">
                        <div class="input-group-append">
                            <span class="input-group-text">pcs</span>
                        </div>
                    </div>
                    @error('leftovers')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="form-group">
                    <div class="form-group" style="display: inline-block; width: 20%;margin-right: 2rem;">
                        <label>Manufacture</label>
                        <select name="manufacture_id" class="form-control">
                            @foreach($manufactures as $manufacture)
                                <option value="{{$manufacture->getKey()}}"
                                        @if ($manufacture['id'] == $instrument['manufacture_id']) selected @endif>
                                    {{$manufacture['name']}}</option>
                            @endforeach
                        </select>

                    </div>
                    <div class="col-lg-12" style="display: inline-block; width: 20%; margin-bottom: 40px;">
                        <div class="form-group">
                            <label>Category</label>
                            <select name="category_id" class="form-control">
                                @foreach($categories as $category)
                                    <option value="{{$category['id']}}"
                                            @if (isset($instrument) && $instrument->categories->contains($category)) selected @endif>
                                        {{$category['name']}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <div class="form-group" style="width: 50%; margin-bottom: 40px;">
                    <label for="images">Images</label>
                    <input type="file" name="images[]" class="form-control" id="images" multiple>
                        <?php if($errors->has('images')): ?>
                    <div class="alert alert-danger"><?= $errors->first('images') ?></div>
                    <?php endif; ?>
                </div>

                <div class="form-group" style=" width: 50%;">
                    <label for="description">Description</label>
                    <textarea name="description"
                              class="form-control"
                              id="description"
                              placeholder="">{{old ('description')}}</textarea>
                    @error('description')
                    <div class="alert alert-danger"> {{$message}}</div>
                    @enderror
                </div>

                <div class="form-group" style=" width: 50%;">
                    <label for="description_ukr">Description UKR</label>
                    <textarea name="description_ukr"
                              class="form-control"
                              id="description_ukr"
                              placeholder="">{{old ('description_ukr')}}</textarea>
                    @error('description_ukr')
                    <div class="alert alert-danger"> {{$message}}</div>
                    @enderror
                </div>
                </br>
                <button type="submit" style="width: 120px" class="btn btn-success">{{__('Save')}}</button>
            </div>
        </form>
    </div>

@endsection


