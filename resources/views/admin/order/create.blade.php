@extends('admin.admin_main')

@section('title-block')
    Order creating form
@endsection

@section('content')

    <form action="{{route('admin.order.store')}}" method="POST">
        @csrf

            <div class="card-body">
                <h1>Order creating form</h1>
                <br>
                <div class="form-group mr-2">
                    <label for="name">Order</label>
                    <label>
                        <input type="text" name="name" value="{{ old('id', $order->id) }}" id="name" class="form-control"
                               placeholder="">
                        @error('name')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </label>
                </div>
                <div class="form-inline">
                    <div class="form-group mr-2">
                        <label for="first_name">First name</label>
                        <input type="text" name="first_name" value="{{ old('first_name', $order->first_name) }}"
                               id="first_name" class="form-control" placeholder="">
                        @error('first_name')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="last_name">Last name</label>
                        <input type="text" name="last_name" value="{{ old('last_name', $order->last_name) }}" id="last_name"
                               class="form-control" placeholder="">
                        @error('last_name')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <br>
                <div class="form-group">
                    <label for="phone">Phone</label>
                    <label>
                        <input type="text"
                               name="phone"
                               value="{{old ('phone', $order->phone)}}" id="phone"
                               class="form-control"
                               placeholder="">
                        @error('phone')
                        <div class="alert alert-danger"> {{$message}}</div>
                        @enderror
                    </label>
                </div>
                @if ($order->user)
                    <div class="form-group">
                        <label for="email">Email</label>
                        <label>
                            <input type="text"
                                   name="email"
                                   value="{{ old('email', $order->user->email) }}"
                                   id="email"
                                   class="form-control"
                                   placeholder="">
                        </label>
                        @error('email')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                @endif
                <div class="form-group">
                    <label for="status">Status</label>
                    <label>
                        <input type="text"
                               name="status"
                               value="{{old ('status', $order->satatus)}}" id="status"
                               class="form-control"
                               placeholder="">
                        @error('status')
                        <div class="alert alert-danger"> {{$message}}</div>
                        @enderror
                    </label>
                </div>
                <h2> Order list</h2>
                <table width="1200">
                    <style type="text/css">
                        th, td {
                            border: 1px solid black;
                        }

                        table th {
                            font-size: 26px;
                        }

                        table td {
                            font-size: 22px;
                        }

                        thead th {
                            text-align: center;
                        }

                        .center {
                            text-align: center;
                        }
                    </style>
        <thead>
        <tr>

            <th colspan="2">Tool id</th>
            <th colspan="2">Tool name</th>
            <th>Quantity</th>
            <th>Price</th>
            <th>Summ</th>
        </tr>
        </thead>
        <tbody id="order-items">
        <tr class="order-item">
            <td colspan="2">
                <input type="id" name="tool_id[]" placeholder="Enter tool id">
            </td>
            <td colspan="2">
                <input type="text" name="tool_name[]" placeholder="Enter tool name">
            </td>
            <td class="center">
                <input type="number" name="quantity[]" min="1" value="1">
            </td>
            <td class="center">
                <input type="number" name="price[]" min="0.01" step="0.01" value="0.00">
            </td>
            <td class="center">0.00</td>
        </tr>
        </tbody>
        <tfoot>
        <tr>
            <td colspan="5">
                <button type="button" class="btn btn-success btn-lg" onclick="addOrderItem()">Add item</button>
                TOTAL AMOUNT: <span id="total-amount">0.00</span> Uah
            </td>
        </tr>
        </tfoot>
        </table>
    <br>
    <button type="submit" class="btn btn-success btn-lg">{{__('Save')}}</button>
{{--    <a href="{{ route('admin.orders.send', ['id' => $order->id]) }}" class="btn btn-info btn-lg">Send order--}}
{{--        table to customer</a>--}}
    </div>
        <script type="text/javascript">
            function addOrderItem() {
                let orderItems = document.getElementById('order-items');
                let orderItemTemplate = `
                    <tr class="order-item">
                        <td colspan="2">
                            <input type="number" name="tool_id[]" placeholder="Enter tool id" oninput="fetchToolData(this)">
                        </td>
                        <td colspan="2">
                            <input type="text" name="tool_name[]" placeholder="Enter tool name" readonly>
                        </td>
                        <td class="center">
                            <input type="number" name="quantity[]" min="1" value="1" onchange="updateTotal()">
                        </td>
                        <td class="center">
                            <input type="number" name="price[]" min="0.01" step="0.01" value="0.00" onchange="updateTotal()">
                        </td>
                        <td class="center">0.00</td>
                    </tr>`;
                orderItems.insertAdjacentHTML('beforeend', orderItemTemplate);
                updateTotal();
            }

            async function fetchToolData(input) {
                const toolId = input.value;
                const url = `/api/instruments/${toolId}`;
                try {
                    const response = await fetch(url);
                    if (response.ok) {
                        const data = await response.json();
                        // действия с полученными данными
                    } else {
                        throw new Error(`Ошибка HTTP: ${response.status}`);
                    }
                } catch (error) {
                    console.error('Ошибка при получении данных:', error);
                }
            }

            </script>
            </form>
@endsection


