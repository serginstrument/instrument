@extends('admin.admin_main')

@section('title-block')
    Orders
@endsection

@section('content')

    <div class="col-lg-12">
        <table class="table table-striped">
            <label>
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Order</th>
                    <th scope="col">Created at</th>
                    <th scope="col">Status</th>
                    <th scope="col">Actions</th>
                </tr>
                </thead>

                <tbody>
                <div class="card-footer">
                    <h1>Orders</h1>
                    <br>
                    <a class="btn btn-outline-success my-2 my-sm-0" style="width: 120px"
                       href="{{route('admin.order.create')}}">Create</a>
                    <label>
                        <div class="container">
                            <form action="{{route('admin.order.index')}}" method="get"
                                  class="d-flex align-items-center justify-content-end w-auto">
                                <div class="input-group">
                                    <input name="search_date"
                                           @if(isset($_GET['search_date'])) value="{{$_GET['search_date']}}"
                                           @endif type="text" class="form-control" placeholder="Search order"
                                           aria-label="Search">
                                </div>
                            </form>
                        </div>
                    </label>
                    <div class="results">Showing <span>{{$orders->total()}}</span> results</div>
                    @foreach ($orders as $order)
                        <tr>
                            <th scope="row">{{$loop->iteration + $orders->perPage()*($orders->currentPage()-1)}}</th>
                            <td>{{ucfirst($order->id)}}</td>
                            <td>{{ucfirst($order->created_at)}}</td>
                            <td>{{ucfirst($order->status)}}</td>
                            <td class="project-actions text-middle">
                                <form action="{{route ('admin.order.destroy', $order)}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <a class="btn btn-info btn-sm wider-btn"
                                       href="{{route ('admin.order.edit', $order)}}">
                                        <i class="fas fa-pencil-alt"></i>
                                        {{__('Edit')}}
                                    </a>

                                    <style>
                                        .wider-btn {
                                            width: 120px;
                                        }
                                    </style>
                                    <button type="submit" class="btn btn-danger btn-sm">
                                        <i class="fas fa-trash"></i>
                                        {{__('Delete')}}
                                    </button>
                                </form>
                            </td>
                        </tr>
                @endforeach
                </tbody>
            </label>
        </table>
        {{$orders->links()}}
    </div>
@endsection
