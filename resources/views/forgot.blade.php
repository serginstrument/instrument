@extends('layouts.main')

@section('title', 'Forgot password')
@section('custom_css')
    <link rel="stylesheet" type="text/css" href="/styles/product.css">
    <link rel="stylesheet" type="text/css" href="/styles/product_responsive.css">
@endsection

@section('content')
    <div class="home">

      <section class="content">
         <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <h3 class="m-0">@lang('login.forgot_password')</h3>
                    <div class="card card-primary">
                        <!-- form start -->
                        <form action="{{route('user.forgot_process')}}" method="POST">
                            @csrf
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="email">@lang('login.email')</label>
                                    <input type="text"
                                           name="email"
                                           value="{{old('email')}}" id="email"
                                           placeholder="@lang('login.enter_email')" class="form-control">
                                    @error('email')
                                    <div class="alert alert-danger"> {{$message}}</div>
                                    @enderror
                                </div>
                                <button type="submit" class="btn btn-success">@lang('login.send')</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
         </div><!-- /.container-fluid -->
      </section>
      </br>
    </div>

@endsection
