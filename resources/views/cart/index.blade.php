@extends('layouts.main')

@section('title', 'Cart')
@section('custom_css')
    <link rel="stylesheet" type="text/css" href="/styles/cart.css">
    <link rel="stylesheet" type="text/css" href="/styles/cart_responsive.css">
@endsection

@section('custom_js')
    <script src="/js/cart.js"></script>
@endsection

@section('content')

    <div class="home">
        <div class="home_container">
            <div class="home_background" style="background-image:url(/images/cart_tool.jpeg)"></div>
            <div class="home_content_container">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <div class="home_content">
                                <div class="breadcrumbs">
                                    <ul>
                                        <li><a href="{{route ('instrument.index')}}">@lang('cart.home')</a></li>
                                        <li>@lang('cart.cart')</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Cart Info -->

    <div class="cart_info">
        <div class="container">
            <div class="row">
                <div class="col">
                    <!-- Column Titles -->
                    <div class="cart_info_columns clearfix">
                        <div class="cart_info_col cart_info_col_product">@lang('cart.product')</div>
                        <div class="cart_info_col cart_info_col_price">@lang('cart.price')</div>
                        <div class="cart_info_col cart_info_col_quantity">@lang('cart.quantity')</div>
                        <div class="cart_info_col cart_info_col_total">@lang('cart.total')</div>
                    </div>
                </div>
            </div>
            @if(!is_null($order))
            @foreach($order->instruments as $instrument)
            <div class="row cart_items_row">

                <div class="col">

                    <!-- Cart Item -->
                    <div class="cart_item d-flex flex-lg-row flex-column align-items-lg-center align-items-start justify-content-start">
                        <!-- Name -->
                        <div class="cart_item_product d-flex flex-row align-items-center justify-content-start">
                            @php
                                $image = '';
                                if (count($instrument->images) > 0){
                                    $image = $instrument->images[0]['img'];
                                } else {
                                $image = '/images/no_image.jpg';
                                }
                            @endphp
                            <div class="product">
                                <div class="product_image">
                                    <img src="{{ count($instrument->images) > 0 ? '/storage/'.$image : '/images/no_image.jpg' }}" width="150" height="150" alt="">
                                </div>
                            </div>
                            <div class="cart_item_name_container">
                                <div class="cart_item_name"><a href="#">{{$instrument->__('name')}}</a></div>
                            </div>
                        </div>
                        <!-- Price -->
                        @if($instrument->new_price != null)
                            <div class="details_price" style="font-weight:inherit;  font-size: 20px; color:#3CB371">{{ number_format($instrument->new_price, 2, '.', ' ') }}UAH</div>
                        @else
                            <div class="details_price" style="font-weight:inherit;  font-size: 20px; color:#3CB371">{{ number_format($instrument->price, 2, '.', ' ') }}UAH</div>
                        @endif
                        <!-- Quantity -->

                        <div class="cart_item_quantity">
                            <div class="product_quantity_container">
                                <div class="product_quantity clearfix">

                                    <label for="quantity_input"></label>
                                    <input id="quantity_input" type="text" pattern="[0-9]*" value="{{$instrument->pivot->count}}">

                                    <div class="quantity_buttons">
                                        <form action="{{route('cart-add', $instrument)}}" method="POST">
                                            @csrf
                                            <button type="submit" class="quantity_inc quantity_control">
                                                <i  class="fa fa-chevron-up" aria-hidden="true"></i>
                                            </button>

                                        </form>

                                        <form action="{{route('cart-remove', $instrument)}}" method="POST">
                                            @csrf
                                            <button type="submit" class="quantity_dec quantity_control">
                                                <i  class="fa fa-chevron-down" aria-hidden="true"></i>
                                            </button>
                                        </form>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="product_price" style="  font-size: 15px; color:#3CB371">{{ number_format($instrument->getPriceForCount(), 2, '.', ' ') }} UAH</div>
                    </div>
                </div>
            </div>
            @endforeach
            <div class="row row_cart_buttons">
                <div class="col">
                    <div class="cart_buttons d-flex flex-lg-row flex-column align-items-start justify-content-start">
                        <div class="button continue_shopping_button"><a href="{{route ('instrument.index')}}">@lang('cart.continue')</a></div>
{{--                        <div class="cart_buttons_right ml-lg-auto">--}}
{{--                            <div class="button clear_cart_button"><a href="#">Clear cart</a></div>--}}
{{--                            <div class="button update_cart_button"><a href="#">Update cart</a></div>--}}
{{--                        </div>--}}
                    </div>
                </div>
            </div>
            <div class="row row_extra">
                <div class="col-lg-4">

                    <!-- Delivery -->
                    <div class="delivery">
                        <div class="section_title">@lang('cart.shipping_method')</div>
                        <div class="section_subtitle">@lang('cart.select')</div>
                        <div class="delivery_options">
                            <label class="delivery_option clearfix">@lang('cart.next_day')
                                <input type="radio" name="radio">
                                <span class="checkmark"></span>
                                <span class="delivery_price">$4.99</span>
                            </label>
                            <label class="delivery_option clearfix">@lang('cart.standard_delivery')
                                <input type="radio" name="radio">
                                <span class="checkmark"></span>
                                <span class="delivery_price">$1.99</span>
                            </label>
                            <label class="delivery_option clearfix">@lang('cart.personal_pickup')
                                <input type="radio" checked="checked" name="radio">
                                <span class="checkmark"></span>
                                <span class="delivery_price">Free</span>
                            </label>
                        </div>
                    </div>

                    <!-- Coupon Code -->
                    <div class="coupon">
                        <div class="section_title">@lang('cart.coupon')</div>
                        <div class="section_subtitle">@lang('cart.enter_coupon')</div>
                        <div class="coupon_form_container">
                            <form action="#" id="coupon_form" class="coupon_form">
                                <label>
                                    <input type="text" class="coupon_input" required="required">
                                </label>
                                <button class="button coupon_button"><span>@lang('cart.apply')</span></button>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6 offset-lg-2">
                    <div class="cart_total">
                        <div class="section_title">@lang('cart.cart_total')</div>
                        <div class="section_subtitle">@lang('cart.final_info')</div>
                        <div class="cart_total_container">
                            <ul>
                                <li class="d-flex flex-row align-items-center justify-content-start">
                                    <div class="cart_total_title">@lang('cart.subtotal')</div>
                                    <div class="cart_total_value ml-auto">{{$order->getFullPrice()}}Uah</div>
                                </li>
                                <li class="d-flex flex-row align-items-center justify-content-start">
                                    <div class="cart_total_title">@lang('cart.shipping')</div>
                                    <div class="cart_total_value ml-auto">Free</div>
                                </li>
                                <li class="d-flex flex-row align-items-center justify-content-start">
                                    <div class="cart_total_title">@lang('cart.total')</div>
                                    <div class="cart_total_value ml-auto">{{$order->getFullPrice()}}Uah</div>
                                </li>
                            </ul>
                        </div>
                        <div class="button checkout_button"><a href="{{route ('cart.place')}}">@lang('cart.proceed')</a></div>
                        @if (session('message'))
                            <div class="alert alert-danger">
                                {{ session('message') }}
                            </div>
                        @endif
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
