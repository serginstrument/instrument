@extends('layouts.main')
@section('title', 'Checkout')
@section('custom_css')
    <link rel="stylesheet" type="text/css" href="/styles/checkout.css">
    <link rel="stylesheet" type="text/css" href="/styles/checkout_responsive.css">
@endsection

@section('custom_js')
    <script src="/js/checkout.js"></script>
@endsection

@section('content')

    <div class="home">
        <div class="home_container">
            <div class="home_background" style="background-image:url(/images/cart_tool.jpeg)"></div>
            <div class="home_content_container">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <div class="home_content">
                                <div class="breadcrumbs">
                                    <ul>
                                        <li><a href="#">@lang('cart.home')</a></li>
                                        <li><a href="#">@lang('cart.cart')</a></li>
                                        <li>Checkout</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Checkout -->
    <form action="{{route ('cart.confirm')}}" method="POST">
        @csrf
        <div class="checkout">
            <div class="container">
                <div class="row">

                    <!-- Billing Info -->
                    <div class="col-lg-6">
                        <div class="billing checkout_section">
                            <div class="section_title">@lang('cart.billing_address')</div>
                            <div class="section_subtitle">@lang('cart.enter_address')</div>
                            <div class="checkout_form_container">
                                <form action="#" id="checkout_form" class="checkout_form">
                                    <div class="row">
                                        <div class="col-xl-6">
                                            <!-- Town -->
                                            <label for="town">City/Town*</label>
                                            <input type="text"
                                                   name="town"
                                                   value="" id="town"
                                                   class="form-control"
                                                   placeholder="">
                                        </div>
                                        <div class="col-xl-6 last_name_col">
                                            <!-- delivery_name -->
                                            <label for="delivery_name">Delivery Name*</label>
                                            <input class="form-control" type="text"
                                                   name="delivery_name"
                                                   value="" id="delivery_name" placeholder="">
                                        </div>
                                    </div>
                                    <div>
                                        <!-- Company -->
                                        {{--                                    <label for="checkout_company">Company</label>--}}
                                        {{--                                    <input type="text" id="checkout_company" class="checkout_input">--}}
                                        {{--                                </div>--}}
                                        {{--                                <div>--}}
                                        {{--                                    <!-- Country -->--}}
                                        {{--                                    <label for="checkout_country">Country*</label>--}}
                                        {{--                                    <select name="checkout_country" id="checkout_country" class="dropdown_item_select checkout_input" require="required">--}}
                                        {{--                                        <option></option>--}}
                                        {{--                                        <option>Lithuania</option>--}}
                                        {{--                                        <option>Sweden</option>--}}
                                        {{--                                        <option>UK</option>--}}
                                        {{--                                        <option>Italy</option>--}}
                                        {{--                                    </select>--}}
                                        {{--                                </div>--}}
                                        {{--                                <div>--}}
                                        {{--                                    <!-- Address -->--}}
                                        {{--                                    <label for="checkout_address">Address*</label>--}}
                                        {{--                                    <input type="text" id="checkout_address" class="checkout_input" required="required">--}}
                                        {{--                                    <input type="text" id="checkout_address_2" class="checkout_input checkout_address_2" required="required">--}}
                                        {{--                                </div>--}}
                                        {{--                                <div>--}}
                                        {{--                                    <!-- Zipcode -->--}}
                                        {{--                                    <label for="checkout_zipcode">Zipcode*</label>--}}
                                        {{--                                    <input type="text" id="checkout_zipcode" class="checkout_input" required="required">--}}
                                        {{--                                </div>--}}
                                        {{--                                <div>--}}
                                        {{--                                    <!-- City / Town -->--}}
                                        {{--                                    <label for="checkout_city">City/Town*</label>--}}
                                        {{--                                    <select name="checkout_city" id="checkout_city" class="dropdown_item_select checkout_input" require="required">--}}
                                        {{--                                        <option></option>--}}
                                        {{--                                        <option>City</option>--}}
                                        {{--                                        <option>City</option>--}}
                                        {{--                                        <option>City</option>--}}
                                        {{--                                        <option>City</option>--}}
                                        {{--                                    </select>--}}
                                        {{--                                </div>--}}
                                        {{--                                <div>--}}
                                        {{--                                    <!-- Province -->--}}
                                        {{--                                    <label for="checkout_province">Province*</label>--}}
                                        {{--                                    <select name="checkout_province" id="checkout_province" class="dropdown_item_select checkout_input" require="required">--}}
                                        {{--                                        <option></option>--}}
                                        {{--                                        <option>Province</option>--}}
                                        {{--                                        <option>Province</option>--}}
                                        {{--                                        <option>Province</option>--}}
                                        {{--                                        <option>Province</option>--}}
                                        {{--                                    </select>--}}
                                        {{--                                </div>--}}
                                        <div>
                                            <!-- Email -->
                                            <label for="delivery_address">Delivery Address*</label>
                                            <input class="form-control" type="text"
                                                   name="delivery_address"
                                                   id="delivery_address"
                                                   placeholder="">
                                        </div>
                                        <div>
                                            {{--                                    <!-- Phone no -->--}}
                                            <label for="phone">Phone no*</label>
                                            <input class="form-control" type="text"
                                                   name="phone"
                                                   id="phone"
                                                   placeholder="">
                                        </div>
                                    {{--                                <div class="checkout_extra">--}}
                                    {{--                                    <div>--}}
                                    {{--                                        <input type="checkbox" id="checkbox_terms" name="regular_checkbox" class="regular_checkbox" checked="checked">--}}
                                    {{--                                        <label for="checkbox_terms"><img src="/images/check.png" alt=""></label>--}}
                                    {{--                                        <span class="checkbox_title">Terms and conditions</span>--}}
                                    {{--                                    </div>--}}
                                    {{--                                    <div>--}}
                                    {{--                                        <input type="checkbox" id="checkbox_account" name="regular_checkbox" class="regular_checkbox">--}}
                                    {{--                                        <label for="checkbox_account"><img src="images/check.png" alt=""></label>--}}
                                    {{--                                        <span class="checkbox_title">Create an account</span>--}}
                                    {{--                                    </div>--}}
                                    {{--                                    <div>--}}
                                    {{--                                        <input type="checkbox" id="checkbox_newsletter" name="regular_checkbox" class="regular_checkbox">--}}
                                    {{--                                        <label for="checkbox_newsletter"><img src="images/check.png" alt=""></label>--}}
                                    {{--                                        <span class="checkbox_title">Subscribe to our newsletter</span>--}}
                                    {{--                                    </div>--}}
                                    {{--                                </div>--}}
                                </form>
                            </div>
                        </div>
                    </div>

                    <!-- Order Info -->

                    <div class="col-lg-6">
                        <div class="order checkout_section">
                            <div class="section_title">@lang('cart.your_order')</div>
                            <div class="section_subtitle">@lang('cart.details')</div>

                            <!-- Order details -->
                            {{--                        <div class="order_list_container">--}}
                            {{--                            <div class="order_list_bar d-flex flex-row align-items-center justify-content-start">--}}
                            {{--                                <div class="order_list_title">Product</div>--}}
                            {{--                                <div class="order_list_value ml-auto">{{$cart->cartCount()}}</div>--}}
                            {{--                            </div>--}}
                            <ul class="order_list">
                                <li class="d-flex flex-row align-items-center justify-content-start">
                                    <div class="order_list_title">@lang('cart.total')</div>
                                    <div class="order_list_value ml-auto">{{$order->getFullPrice()}}Uah</div>
                                </li>
                            </ul>
                        </div>

                        <!-- Payment Options -->
                        <div class="payment">
                            <div class="payment_options">
                                <label class="payment_option clearfix">Paypal
                                    <input type="radio" name="radio">
                                    <span class="checkmark"></span>
                                </label>
                                <label class="payment_option clearfix">Cach on delivery
                                    <input type="radio" name="radio">
                                    <span class="checkmark"></span>
                                </label>
                                <label class="payment_option clearfix">Credit card
                                    <input type="radio" name="radio">
                                    <span class="checkmark"></span>
                                </label>
                                <label class="payment_option clearfix">Direct bank transfer
                                    <input type="radio" checked="checked" name="radio">
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>

                        <!-- Order Text -->

                        <div class="order_text">@lang('cart.order_accepted')</div>

                        {{--                        <div class="button order_button"><a href="{{route ('cart.confirm')}}">Place Order</a></div>--}}
                        <button type="submit" class="btn btn-primary" role="button">@lang('cart.place_order')</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

