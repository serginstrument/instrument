<?php

namespace App\Console\Commands;

use App\Imports\InstrumentsImport;
use Illuminate\Console\Command;
use Maatwebsite\Excel\Facades\Excel;

class ImportExcelCommand extends Command
{

    protected $signature = 'import:excel';

    protected $description = 'Get data from excel';

    /**
     * Execute the console command.
     */
    public function handle()
    {
//        ini_set('memory_limit', '-1'); //выключает ограничение памяти

        Excel::import(new InstrumentsImport(), public_path('excel/Tools.xlsx'));

        dd('finish');
    }

}
