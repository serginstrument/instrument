<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class InstrumentImage extends Model
{
    use HasFactory;
    protected $table = 'instrument_images';
    protected $primaryKey = 'id';
    protected $fillable = [
        'instrument_id',
        'img',
     ];
    public function getImagesAttribute()
    {
        return asset('storage/' . $this->img);
    }
    public function instrument(): BelongsTo
    {
        return $this->belongsTo(Instrument::class, 'instrument_id', 'id');
    }


}
