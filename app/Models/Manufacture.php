<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Manufacture extends Model
{
    use HasFactory;

    protected $primaryKey = 'id';
    protected $table = 'manufactures';
    protected $fillable = [
        'name',
    ];

    public function instruments(): HasMany
    {
        return $this->hasMany(Instrument::class, 'manufacture_id', 'id');
    }
}
