<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Role extends Model
{
    protected $table = 'roles';
    const DEFAULT_ROLE = '3';
    protected $fillable = [
        'slug',
        'name',
    ];

    public function permissions()
    {
        return $this
            ->belongsToMany(Permission::class, 'role_permission', 'role_id', 'permission_id');
    }

    public function users()
    {
        return $this
            ->belongsToMany(User::class, 'user_role', 'user_id', 'role_id');
    }
}
