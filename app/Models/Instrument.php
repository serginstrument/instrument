<?php

namespace App\Models;

use App\Filters\QueryFilter;
use App\Services\CurrencyConvertionService;
use App\Traits\Translateble;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Instrument extends Model
{
    use HasFactory;
    use Translateble;

    protected $table = 'instruments';
    protected $primaryKey = 'id';
    protected $with = ['manufacture', 'images'];

    protected $fillable = [
        'name',
        'manufacture_id',
        'price',
        'new_price',
        'leftovers',
        'description',
        'img',
        'name_ukr',
        'description_ukr',
    ];

    public function scopeFilter(Builder $builder, QueryFilter $filter)
    {

        return $filter->apply($builder);
    }


    public function categories(): BelongsToMany
    {
        return $this->belongsToMany(Category::class);
    }

    public function manufacture(): BelongsTo
    {
        return $this->belongsTo(Manufacture::class, 'manufacture_id', 'id');
    }

    public function images(): hasMany
    {
        return $this->hasMany(InstrumentImage::class);
    }

    public function orders(): BelongsToMany
    {
        return $this->belongsToMany(Order::class);
    }

    public function instrumentOrders(): BelongsToMany
    {
        return $this->belongsToMany(InstrumentOrder::class);
    }

    public function getPriceForCount()
    {
        if ($this->new_price != null) {
            if (!is_null($this->pivot)) {
                return $this->pivot->count * $this->new_price;
            }
            return $this->new_price;
        } else {
            if (!is_null($this->pivot)) {
                return $this->pivot->count * $this->price;
            }
            return $this->price;
        }

    }
    public function getPriceAttribute($value)
    {
        return round(CurrencyConvertionService::convert($value), 2);
    }

    public function getNewPriceAttribute($value)
    {
        return CurrencyConvertionService::convert($value);
    }

}
