<?php

namespace App\Models;

class InstrumentOrder
{
    public function instruments()
    {
        return $this->belongsToMany(Instrument::class);
    }
}
