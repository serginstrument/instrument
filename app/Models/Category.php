<?php

namespace App\Models;

use App\Traits\Translateble;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;
    use Translateble;

    public $table = 'categories';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name',
        'image',
        'description',
        'name_ukr',
        'description_ukr',
    ];

    public function instruments()
    {
        return $this->belongsToMany(Instrument::class);
    }
}
