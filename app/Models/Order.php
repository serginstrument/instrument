<?php

namespace App\Models;

use App\Filters\QueryFilter;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $table = 'orders';

    protected $fillable = [
        'first_name',
        'last_name',
        'user id',
        'phone',
    ];

    protected $attributes = [
        'first_name' => '',
        'last_name' => '',

    ];
    public function scopeFilter(Builder $builder, QueryFilter $filter){

        return $filter->apply($builder);
    }

//    protected $table = 'orders';
    public function instruments()
    {
//        protected $table = 'orders';
//        $order = Order::find(1);
        return $this->belongsToMany(Instrument::class)->withPivot('count')->withTimestamps();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getFullPrice()
    {
        $summ = 0;
        foreach ($this->instruments as $instrument) {
            $summ += $instrument->getPriceForCount();
        }
        return $summ;
    }

    public function getInstrumentCount()
    {
        $summ1 = 0;
        foreach ($this->instruments as $instrument) {
            $summ1 += $instrument->count;
        }
        return $summ1;
    }
}
