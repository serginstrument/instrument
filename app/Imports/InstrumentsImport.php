<?php

namespace App\Imports;

use App\Models\Instrument;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class InstrumentsImport implements ToCollection, WithHeadingRow
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        foreach ($collection as $item){
            if (isset ($item['name']) && $item['name'] != null)
            {
                Instrument::firstOrCreate([
                    'name'=> $item['name'],
                ],
                    [
                      'name'=> $item['name'],
                      'manufacture_id'=> $item['manufacture_id'],
                      'price'=> $item['price'],
                      'new_price'=> $item['new_price'],
                      'description'=> $item['description'],
                      'leftovers'=> $item['leftovers'],
                ]);
            }
        }
    }
}
