<?php

namespace App\Imports;

use App\Models\User;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class UsersImport implements ToCollection, WithHeadingRow
{
    public function collection(Collection $collection)
    {
        foreach ($collection as $item){
            if (isset ($item['first_name']) && $item['first_name'] != null)
            {
                User::firstOrCreate([
                    'first_name'=> $item['first_name'],
                ],
                    [
                        'first_name'=> $item['first_name'],
                        'last_name'=> $item['last_name'],
                        'phone'=> $item['phone'],
                        'email'=> $item['email'],
                        'password'=> $item['password'],
                    ]);
            }
        }
    }
}
