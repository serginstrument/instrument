<?php

namespace App\Http\Controllers;

use App\Http\Requests\Admin\OrderRequest;
use App\Models\Order;
use App\Services\CartService;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{
    protected $cartService;

    public function __construct(CartService $cartService)
    {
        $this->cartService = $cartService;
    }

    public function index()
    {
        $orderId = session('orderId');
        $order = null;

        if (!is_null($orderId)) {
            $order = Order::findOrFail($orderId);
        }
        return view('cart.index', compact('order'));
    }

    public function cartConfirm(OrderRequest $request)
    {
        $orderId = session('orderId');
        if (is_null($orderId)) {
            return redirect()->route('instrument.index');
        }

        $this->cartService->confirm($orderId, $request->town, $request->delivery_address, $request->delivery_name);

        return redirect()->route('instrument.index');
    }
    public function cartPlace()
    {
        $orderId = session('orderId');
        if (is_null($orderId)) {
            return redirect()->route('instrument.index');
        }

        if ($user = Auth::user()) {

            $firstName = $user->first_name;
            $lastName = $user->last_name;
            $phone = $user->phone;

            $order = $this->cartService->place($orderId, $user->id, $firstName, $lastName, $phone);

            return view('cart.place', compact('order'));

        }else{
            return redirect()->back()->with('message', 'Please log in to place your order.');
        }
    }
    public function cartAdd($instrumentId)
    {
        $this->cartService->add($instrumentId);
        return redirect()->route('cart.index');
    }

    public function cartRemove($instrumentId)
    {
        $this->cartService->remove($instrumentId);
        return redirect()->route('cart.index');
    }

    public function cartCount()
    {
        $count = $this->cartService->getInstrumentsCount();
        return $count;
    }
}
