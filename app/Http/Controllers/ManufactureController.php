<?php

namespace App\Http\Controllers;

use App\Models\Manufacture;
use Illuminate\Http\Request;

class ManufactureController extends Controller
{
    public function show(int $id)
    {
        $manufacture = Manufacture::query()
            ->where('id', $id)
            ->first();
        return $manufacture;

    }
}
