<?php

namespace App\Http\Controllers;

use App\Mail\ForgotPassword;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        if (Auth::check()) {
            return redirect()->intended(route('instrument.index'));
        }
        $formFields = $request->only(['email', 'password']);

        if (Auth::attempt($formFields)) {
            return redirect()->intended(route('instrument.index'));
        }
        return redirect(route('user.login'))->withErrors([
            'email' => 'Не удалось авторизироваться'
        ]);
    }

    public function logout()
    {
        auth("web")->logout();

        return redirect(route("user.login"));
    }

    public function showForgotForm()
    {
        return view("/forgot");
    }

    public function forgot(Request $request)
    {
        $data = $request->validate([
            "email" => ["required", "email", "string", "exists:users"],
        ]);

        $user = User::where(["email" => $data["email"]])->first();

        $password = uniqid();
        $user->password = Hash::make($password);
        $user->save();

        Mail::to($user)->send(new ForgotPassword($password));

        return redirect(route("user.login"));
    }
}
