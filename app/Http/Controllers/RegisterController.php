<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegisterRequest;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class RegisterController extends Controller
{

    public function save(RegisterRequest $request)
    {
        if (Auth::check()) {
            return redirect(route('instrument.index'));
        }
        $validateFields = $request->validated();

        if (User::where('email', $validateFields['email'])->exists()) {
            return redirect(route('user.registration'))->withErrors([
                'email' => 'This user is already registered'
            ]);
        }
        $user = User::create($validateFields);

        $user->roles()->attach(3);

        if ($user) {
            Auth::login($user);
            return redirect(route('instrument.index'));
        }
        return redirect(route('user.login'))->withErrors([
            'formError' => 'An error occurred while saving the user'
        ]);
    }
}
