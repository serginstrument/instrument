<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CategoryRequest;
use App\Models\Category;
use App\Services\Admin\CategoryService;
use Illuminate\Support\Facades\Storage;

class CategoryController extends Controller
{
    private $categoryService;

    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    public function index()
    {
        $categories = Category::query()->paginate(10);

        return view('admin.category.index')->with(['categories' => $categories]);
    }

    public function create()
    {
        return view('admin.category.create');
    }

    public function store(CategoryRequest $request)
    {
        if ($this->categoryService->storeCategory($request)) {
            return redirect('admin_panel/category')->with('success', 'Category created successfully');
        }

        return redirect()->back()->with('error', 'Failed to upload image');
    }

    public function edit(Category $category)
    {
        return view('admin.category.edit')->with(compact('category'));
    }

    public function update(CategoryRequest $request, Category $category)
    {
        if ($this->categoryService->updateCategory($request, $category)) {
            return redirect()->route('admin.category.index')->with('success', 'Category updated successfully');
        }

        return redirect()->back()->with('error', 'Failed to upload image');
    }

    public function destroy(Category $category)
    {
        if (!$this->categoryService->deleteCategory($category)) {
            return redirect()->route('admin.category.index')
                ->with('error', 'Cannot delete category with associated instruments');
        }

        return redirect()->route('admin.category.index')->with('success', 'Category deleted successfully');
    }
}

