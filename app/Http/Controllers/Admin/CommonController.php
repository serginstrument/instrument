<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CommonController extends Controller
{
    public function destroyWithRedirect($model, $currentPage, $redirectRoute, $successMessage)
    {
        $model->delete();

        $totalModels = $model::count();
        $perPage = 10; // Замените на свое значение пагинации

        if (($currentPage - 1) * $perPage >= $totalModels && $currentPage > 1) {
            $previousPage = $currentPage - 1;
            return redirect()->route($redirectRoute, ['page' => $previousPage])->with('success', $successMessage);
        }

        return redirect()->route($redirectRoute, ['page' => $currentPage])->with('success', $successMessage);
    }

}
