<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\RoleRequest;
use App\Models\Permission;
use App\Models\Role;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    public function index()
    {
        $roles = Role::query()->paginate(10);

        return view('admin.role.index')->with(['roles' => $roles]);
    }

    public function create()
    {
        $permissions = Permission::all();
        $role = new Role();

        return view('admin.role.create')->with(compact('role', 'permissions'));
    }

    public function store(RoleRequest $request)
    {
        $input = $request->validated();

        $role = Role::create($input);
        $role->permissions()->attach($request->permissions);

        return redirect('admin_panel/role')->with(compact('role'));
    }

    public function edit(Role $role)
    {
        $permissions = Permission::all();

        return view('admin.role.edit')->with(compact('role', 'permissions'));
    }

    public function update(RoleRequest $request, Role $role)
    {
        $role->update($request->validated());

        $role->permissions()->sync($request->permissions);

        return redirect()->route('admin.role.index');
    }

    public function destroy(Role $role)
    {
        $role->delete();

        return redirect()->route('admin.role.index');
    }
}
