<?php

namespace App\Http\Controllers\Admin;

use App\Exports\InstrumentsExport;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class InstrumentExportController extends Controller
{
    public function export()
    {
        session()->flash('success', 'Data exported successfully');
        return Excel::download(new InstrumentsExport(), 'instruments.xlsx');
    }
}
