<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Imports\UsersImport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class UserImportController extends Controller
{
    public function import(Request $request)
    {

        if ($request->hasFile('users')) {
            $file = $request->file('users');
            if ($file->getClientOriginalExtension() == 'xlsx') {
                Excel::import(new UsersImport, $file, 'Xlsx');

                return redirect('admin_panel/user')->with('success', 'Data imported successfully');

            } else {

                return redirect('admin_panel/user')->with('error', 'Invalid file type. Only .xlsx files are allowed.');
            }
        } else {

            return redirect('admin_panel/user')->with('error', 'File not found');
        }
    }
}
