<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\ManufactureResource;
use App\Models\Manufacture;
use Illuminate\Http\Request;

class ManufactureController extends Controller
{
    public function index()
    {
        $manufactures = Manufacture::query()->paginate(10);

//        return ManufactureResource::collection($manufactures);

        return view('admin.manufacture.index')->with(['manufactures' => $manufactures]);
    }

    public function create()
    {
        return view('admin.manufacture.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $manufacture = Manufacture::create(
            $request->only((new Manufacture())->getFillable())
        );

//        return new ManufactureResource($manufacture);
        return redirect('admin_panel/manufacture')->with(compact('manufacture'))
            ->with('success', 'Manufacture created successfully');
    }

    public function edit(Manufacture $manufacture)
    {
        return view('admin.manufacture.edit')->with(compact('manufacture'));
    }

    public function update(Request $request, Manufacture $manufacture)
    {
        $manufacture->update($request->only((new Manufacture())->getFillable()));

//        return new ManufactureResource($manufacture);

        return redirect()->route('admin.manufacture.index')
            ->with('success', 'Manufacture updated successfully');
    }

    public function destroy(Manufacture $manufacture)
    {
        if ($manufacture->instruments()->count() > 0) {
            return redirect()->route('admin.manufacture.index')
                ->with('error', 'Cannot delete manufacture with associated instruments');
        }
        $manufacture->delete();

        return redirect()->route('admin.manufacture.index')
            ->with('success', 'Manufacture deleted successfully');
    }
}
