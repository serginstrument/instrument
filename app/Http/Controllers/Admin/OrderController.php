<?php

namespace App\Http\Controllers\Admin;

use App\Filters\OrderFilter;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\OrderRequest;
use App\Mail\OrderTableMail;
use Illuminate\Http\Request;
use App\Models\InstrumentOrder;
use App\Models\Order;
use App\Models\User;
use Illuminate\Support\Facades\Mail;

class OrderController extends Controller
{

    public function index(OrderFilter $request)
    {
        $orders = Order::filter($request)
            ->orderByDesc('created_at')
            ->paginate(10);

        return view('admin.order.index')->with(['orders' => $orders]);
    }

    public function create()
    {
        $order = new Order();

        return view('admin.order.create')->with('order', $order);
    }


    public function store(OrderRequest $request)
    {
        $data = $request->validated();

        $order = new Order($data);

        $order->save();

        return redirect()->route('admin.order.index')
            ->with('success', 'Order created successfully.');
    }

    public function show($id)
    {
        $order = Order::findOrFail($id);
        return view('admin.order.show')->with('order', $order);
    }

    public function edit(Order $order)
    {
        $user = User::all();
        return view('admin.order.edit')->with(compact('order', 'user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */

    public function update(OrderRequest $request, Order $order)
    {
        // Получаем данные из запроса
        $orderData = $request->validated();
        // Обновляем заказ в базе данных
        $order->update($orderData);
        $order->status = 2;

        $instrumentOrders = $request->input('instrument_orders') ?? [];

        // Обновляем заказанные инструменты
        foreach ($instrumentOrders as $instrumentOrderData) {
            $instrumentOrder = InstrumentOrder::where([
                'order_id' => $order->id,
                'instrument_id' => $instrumentOrderData['instrument_id'],
            ])->firstOrFail();

            $instrumentOrder->update([
                'count' => $instrumentOrderData['count'],
            ]);
        }

        // Сохраняем изменения в инструментах
        foreach ($order->instruments as $instrument) {
            $instrument->save();
        }

        // Обновляем заказ в базе данных еще раз для сохранения изменений в инструментах
        $order->save();

        // Перенаправляем пользователя на страницу редактирования заказа
        return redirect()->route('admin.order.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Order $order)
    {
        $order->delete();
        return redirect()->route('admin.order.index');
    }

    public function sendOrderTableToCustomer($id)
    {

        // Получаем заказ из базы данных по его ID
        $order = Order::with('user', 'instruments')->findOrFail($id);

        // Создаем массив для хранения строк таблицы
        $rows = [];

        // Добавляем заголовок таблицы
        $rows[] = ['Tool name', 'Quantity', 'Price', 'Summ'];

        // Добавляем строки с товарами
        $totalSumm = 0;

        if ($order->instruments) {
            foreach ($order->instruments as $instrument) {
                $summ = $instrument->pivot->count * $instrument->price;
                $rows[] = [$instrument->name, $instrument->pivot->count, $instrument->price, $summ];
                $totalSumm += $summ;
            }
        }
// Добавляем строку с общей суммой
        $rows[] = ['', '', 'TOTAL AMOUNT DUE =', $totalSumm . ' Uah'];

// Генерируем HTML-таблицу
        $table = '<table border="1">';
        foreach ($rows as $row) {
            $table .= '<tr>';
            foreach ($row as $cell) {
                $table .= '<td>' . $cell . '</td>';
            }
            $table .= '</tr>';
        }
        $table .= '</table>';

        $order->update(['status' => 2]);
// Отправляем сообщение пользователю с таблицей заказа в качестве вложения
        Mail::to($order->user->email)
            ->send(new OrderTableMail($table));

// Возвращаемся на страницу заказов с сообщением об успешной отправке таблицы
        return redirect()->route('admin.order.index')->with('success', 'The order table has been exported and sent to the customer.');

    }

}
