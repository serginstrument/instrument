<?php

namespace App\Http\Controllers\Admin;

use App\Exports\UsersExport;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class UserExportController extends Controller
{
    public function export()
    {
        session()->flash('success', 'Data exported successfully');
        return Excel::download(new UsersExport(), 'users.xlsx');
    }
}
