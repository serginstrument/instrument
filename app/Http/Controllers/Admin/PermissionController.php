<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\PermissionRequest;
use App\Models\Permission;
use Illuminate\Http\Request;

class PermissionController extends Controller
{

    public function index()
    {
        $permissions = Permission::query()->paginate(10);
        return view('admin.permission.index')->with(['permissions' => $permissions]);
    }

    public function create()
    {
        return view('admin.permission.create');
    }

    public function store(PermissionRequest $request)
    {

        $permission = Permission::create(
            $request->only((new Permission())->getFillable())
        );

        return redirect('admin_panel/permission')->with(compact('permission'));
    }

    public function show($id)
    {
        //
    }

    public function edit(Permission $permission)
    {
        return view('admin.permission.edit')->with(compact('permission'));
    }

    public function update(PermissionRequest $request, Permission $permission)
    {
        $permission->update($request->only((new Permission())->getFillable()));
        return redirect()->route('admin.permission.index');
    }

    public function destroy(Permission $permission)
    {
        $permission->delete();
        return redirect()->route('admin.permission.index');
    }
}
