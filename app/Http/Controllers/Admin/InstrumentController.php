<?php

namespace App\Http\Controllers\Admin;

use App\Http\Resources\InstrumentResource;
use App\Services\Admin\InstrumentService;
use Illuminate\Support\Facades\Storage;
use App\Filters\InstrumentFilter;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\InstrumentRequest;
use App\Models\Category;
use App\Models\Instrument;
use App\Models\InstrumentImage;
use App\Models\Manufacture;
use Illuminate\Http\Request;

class InstrumentController extends Controller
{
    public InstrumentService $service;
    protected $commonController;

    public function __construct(CommonController $commonController, InstrumentService $service)
    {
        $this->commonController = $commonController;
        $this->service = $service;
    }

    public function index(InstrumentFilter $request)
    {
        $instruments = Instrument::filter($request)->paginate(10);

//        return InstrumentResource::collection($instruments);

        return view('admin.instrument.index')->with(['instruments' => $instruments]);
    }

    public function show(Instrument $instrument)
    {
        return new InstrumentResource($instrument);
    }

    public function create()
    {
        $manufactures = Manufacture::all();
        $categories = Category::all();

        $instrument = new Instrument();

        return view('admin.instrument.create')
            ->with(compact('manufactures', 'categories', 'instrument'));
    }

    public function store(InstrumentRequest $request)
    {
        $instrument = Instrument::create($request->all());

        $category_ids = $request->get('category_id');
        $instrument->categories()->attach($category_ids);

        $images = $request->file('images');

        $this->service->storeImages($instrument, $images);

//        return new InstrumentResource($instrument);
        return redirect('admin_panel/instrument')->with(compact('instrument'))
            ->with('success', 'Instrument created successfully');
    }

    public function edit($id)
    {
        $instrument = Instrument::with('images')->findOrFail($id);
        $manufactures = Manufacture::all();
        $categories = Category::all();
        $instrumentImages = $instrument->images;

        return view('admin.instrument.edit')
            ->with(compact('instrument', 'manufactures', 'categories', 'instrumentImages'));
    }

    public function update(InstrumentRequest $request, Instrument $instrument)
    {
        $instrument->fill($request->all())->update();

        $category_ids = $request->get('category_id');
        $instrument->categories()->sync($category_ids);

        $images = $request->file('images');
        $this->service->updateImages($instrument, $images);

        $deleteImages = $request->input('delete_image');
        $this->service->deleteAllImages($deleteImages);
//        return new InstrumentResource($instrument);
        return redirect()->route('admin.instrument.index')->with('success', 'Instrument updated successfully');
    }

    public function destroy(Instrument $instrument, Request $request)
    {
        $currentPage = $request->query('page', 1);
        return $this->commonController->destroyWithRedirect($instrument, $currentPage, 'admin.instrument.index', 'Instrument deleted successfully');
    }
}

