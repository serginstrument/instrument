<?php

namespace App\Http\Controllers\Admin;

use App\Filters\UserFilter;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\UserRequest;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;


class UserController extends Controller
{
    protected $commonController;

    public function __construct(CommonController $commonController)
    {
        $this->commonController = $commonController;
    }

    public function index(UserFilter $request)
    {
        $users = User::filter($request)->paginate(10);

        return view('admin.user.index')->with(['users' => $users]);
    }

    public function create()
    {
        $roles = Role::all();

        return view('admin.user.create')->with(compact('roles'));
    }

    public function store(UserRequest $request)
    {
        $input = $request->validated();
        $user = User::create($input);

        $role_ids = $request->get('role_id');
        $user->roles()->attach($role_ids);

        return redirect('admin_panel/user')->with(compact('user'))
            ->with('success', 'User created successfully');
    }

    public function edit(User $user)
    {
        $roles = Role::all();

        return view('admin.user.edit')->with(compact('user', 'roles'));
    }

    public function update(Request $request, User $user)
    {
        $user->update($request->all());

        $role_ids = $request->get('role_id');
        $user->roles()->sync($role_ids);

        return redirect()->route('admin.user.index')->with('success', 'User updated successfully');
    }

    public function destroy(User $user, Request $request)
    {
        $currentPage = $request->query('page', 1);
        return $this->commonController->destroyWithRedirect($user, $currentPage, 'admin.user.index', 'User deleted successfully');
    }
}
