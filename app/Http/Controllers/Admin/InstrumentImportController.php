<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Imports\InstrumentsImport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class InstrumentImportController extends Controller
{
    public function import(Request $request)
    {
        if ($request->hasFile('instruments')) {
            $file = $request->file('instruments');
            if ($file->getClientOriginalExtension() == 'xlsx') {
                Excel::import(new InstrumentsImport, $file, 'Xlsx');

                return redirect('admin_panel/instrument')->with('success', 'Data imported successfully');

            } else {

                return redirect('admin_panel/instrument')->with('error', 'Invalid file type. Only .xlsx files are allowed.');
            }
        } else {

            return redirect('admin_panel/instrument')->with('error', 'File not found');
        }
    }

}
