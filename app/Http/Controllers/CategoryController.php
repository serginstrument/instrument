<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{

    public function index()
    {

        $categories = Category::query()->paginate(10);

        return view('category.index')->with(['categories' => $categories]);
    }


    public function show(Category $category)
    {
        $instruments = $category->instruments()->paginate(10);

        return view('category.show')->with(compact('category', 'instruments'));
    }
}
