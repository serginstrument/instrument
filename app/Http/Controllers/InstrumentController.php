<?php

namespace App\Http\Controllers;

use App\Filters\InstrumentFilter;
use App\Models\Instrument;


class InstrumentController extends Controller
{

    public function index(InstrumentFilter $request)
    {
        $instruments = Instrument::filter($request)->paginate(20);

        return view('instrument.index')->with(['instruments' => $instruments]);
    }

    public function show($id)
    {
        $instrument = Instrument::with('manufacture', 'images')->findOrFail($id);

        $categoryIds = $instrument->categories()->pluck('id');

        $recommendedInstruments = Instrument::where('id', '!=', $id)
            ->whereHas('categories', function ($query) use ($categoryIds) {
                $query->whereIn('id', $categoryIds);
            })
            ->inRandomOrder()
            ->limit(4)
            ->get();

        return view('instrument.show', compact('instrument', 'recommendedInstruments'));
    }
}
