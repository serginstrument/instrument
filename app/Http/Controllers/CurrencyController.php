<?php

namespace App\Http\Controllers;

use App\Models\Currency;


class CurrencyController extends Controller
{
    public function changeCurrency($currencyCode)
    {
        $currency = Currency::byCode($currencyCode)->firstOrFail();
        session(['currency' => $currency->code]);
        return redirect()->back();
   }
}
