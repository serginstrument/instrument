<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class LocaleController extends Controller
{
    public function changeLocale($locale)
    {
        $availableLocales = ['en', 'ukr'];
        if (!in_array($locale, $availableLocales)){
            $locale = config('app.locale');
        }
        session(['locale' => $locale]);
        App::setlocale($locale);
        return redirect()->back();
    }
}
