<?php

namespace App\Exports;

use App\Models\Instrument;
use Maatwebsite\Excel\Concerns\FromCollection;

class InstrumentsExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
       return Instrument::all();
    }
}
