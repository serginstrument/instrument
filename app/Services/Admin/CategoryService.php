<?php
namespace App\Services\Admin;

use App\Http\Requests\Admin\CategoryRequest;
use App\Models\Category;
use Illuminate\Support\Facades\Storage;

class CategoryService
{
    public function storeCategory(CategoryRequest $request)
    {
        $image = $request->file('image');

        if ($image && $image->isValid()) {
            $path = $this->storeImage($image, 'categories');
            $params = $request->all();
            $params['image'] = $path;
            Category::create($params);

            return true;
        }

        return false;
    }

    public function updateCategory(CategoryRequest $request, Category $category)
    {
        if ($category->image) {
            Storage::delete($category->image);
        }

        $image = $request->file('image');

        if ($image && $image->isValid()) {
            $path = $this->storeImage($image, 'categories');
            $params = $request->all();
            $params['image'] = $path;
            $category->update($params);

            return true;
        }

        return false;
    }

    public function deleteCategory(Category $category)
    {
        if ($category->instruments()->count() > 0) {
            return false;
        }

        if ($category->image) {
            Storage::delete($category->image);
        }

        $category->delete();

        return true;
    }

    protected function storeImage($image, $directory)
    {
        return $image->store($directory);
    }
}
