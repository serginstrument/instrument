<?php

namespace App\Services\Admin;


use App\Http\Requests\Admin\InstrumentRequest;
use App\Models\Instrument;
use App\Models\InstrumentImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class InstrumentService
{
    public function storeImages($instrument, array $images)
    {
        foreach ($images as $image) {
            $path = $image->store('instruments');
            $instrumentImage = new InstrumentImage([
                'img' => $path,
                'instrument_id' => $instrument->id,
            ]);
            $instrumentImage->save();
        }
    }

    public function updateImages($instrument, $images)
    {
        if ($images) {
            foreach ($images as $image) {
                $path = $image->store('instruments');
                $instrumentImage = new InstrumentImage([
                    'img' => $path,
                    'instrument_id' => $instrument->id,
                ]);
                $instrumentImage->save();
            }
        }
    }

    public function deleteAllImages($deleteImages)
    {
        if ($deleteImages) {
            $this->deleteImage($deleteImages);
        }
    }

    private function deleteImage($ids)
    {
        if (!is_array($ids)) {
            $ids = [$ids];
        }

        foreach ($ids as $id) {
            $image = InstrumentImage::findOrFail($id);
            $image_path = public_path('storage/' . $image->img);

            if (file_exists($image_path)) {
                unlink($image_path);
            }
            $image->delete();
        }
    }
}
