<?php

namespace App\Services;

use App\Models\Currency;
use Exception;
use GuzzleHttp\Client;


class CurrencyRateService
{
    public static function getRates()
    {
        $baseCurrency = CurrencyConvertionService::getBaseCurrency();

        $url = config('currency_rates.api_url') . '?base=' . $baseCurrency->code;

        $client = new Client();

        $response = $client->request('GET', $url);
        if ($response->getStatusCode() !== 200) {
            throw new Exception('There is a problem with currency rate');
        }
        $rates = json_decode($response->getBody()->getContents(), true)['rates'];

        foreach (CurrencyConvertionService::getCurrencies() as $currency) {
            if (!$currency->isMian()) {
                if (!isset($rates[$currency->code])) {
                    throw new Exception('There is a problem with currency $currency->code');
                }else {
                    $currency->update(['rate' => $rates[$currency->code]]);
                    $currency->touch();
                }
            }
        }
    }
}
