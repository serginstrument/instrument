<?php

namespace App\Services;

use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CartService
{
    public function confirm(Request $request)
    {
        $orderId = session('orderId');
        if (is_null($orderId)) {
            return redirect()->route('instrument.index');
        }
        $order = Order::find($orderId);
        $order->first_name = $request->first_name;
        $order->last_name = $request->last_name;
        $order->phone = $request->phone;
        $order->status = 1;
        $order->user_id = Auth::id();
        $order->save();
        session()->forget('orderId');
        return $order;
    }

    public function place($orderId, $userId, $firstName, $lastName, $phone)
    {

        $order = Order::findOrFail($orderId);

        $order->user_id = $userId;
        $order->first_name = $firstName;
        $order->last_name = $lastName;
        $order->phone = $phone;

        $order->save();

        session()->forget('orderId');

        return $order;
    }

    public function add($instrumentId)
    {
        $orderId = session('orderId');

        if (is_null($orderId)) {
            $order = Order::create();
            session(['orderId' => $order->id]);
        } else {
            $order = Order::find($orderId);
        }

        if (is_null($order)) {
            throw new \Exception('Order not found');
        }

        if ($order->instruments->contains($instrumentId)) {
            $pivotRow = $order->instruments()
                ->where('instrument_id', $instrumentId)
                ->first()->pivot;
            $pivotRow->count++;
            $pivotRow->update();
        } else {
            $order->instruments()->attach($instrumentId);
        }

        return $order;
    }

    public function remove($instrumentId)
    {
        $orderId = session('orderId');
        if (is_null($orderId)) {
            return redirect()->route('cart.index');
        }
        $order = Order::find($orderId);

        if ($order->instruments->contains($instrumentId)) {
            $pivotRow = $order->instruments()
                ->where('instrument_id', $instrumentId)
                ->first()->pivot;
            if ($pivotRow->count < 2) {
                $order->instruments()->detach($instrumentId);
            } else {
                $pivotRow->count--;
                $pivotRow->update();
            }
        }
        return $order;
    }

    public function getCartInstruments()
    {
        $orderId = session('orderId');
        if (is_null($orderId)) {
            return collect();
        }

        $order = Order::find($orderId);

        return $order ? $order->instruments : collect();
    }
}
