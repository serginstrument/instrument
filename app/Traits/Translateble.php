<?php

namespace App\Traits;

use Illuminate\Support\Facades\App;

trait Translateble
{
    protected string $defaultLocale = 'en';

    public function __($originFieldName)
    {
        $locale = App::getLocale() ?? $this->defaultLocale;

        if ($locale === 'ukr') {
            $fieldName = $originFieldName . '_ukr';
        } else {
            $fieldName = $originFieldName;
        }

        $attributes = array_keys($this->attributes);

        if (!in_array($fieldName, $attributes)) {
            throw new \LogicException('no such attribute for model ' . get_class($this));
        }

        if ($locale === 'ukr' && (is_null($this->$fieldName) || empty($this->$fieldName))) {
            return $this->$originFieldName;
        }

        return $this->$fieldName;
    }

}
