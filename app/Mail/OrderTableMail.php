<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderTableMail extends Mailable
{
    use Queueable, SerializesModels;

    public $table;

    public function __construct($table)
    {
        $this->table = $table;
    }

    public function build()
    {
        return $this->view('email.order-table')
            ->subject('Your order table')
            ->attachData($this->table, 'order-table.html');
    }
}
