<?php

namespace App\Filters;

class UserFilter extends QueryFilter
{
    public function search_full_name($search_string = '')
    {
        return $this->builder
            ->where('first_name', 'LIKE', '%'.$search_string.'%')
            ->orwhere('last_name', 'LIKE', '%'.$search_string.'%' );
    }
}
