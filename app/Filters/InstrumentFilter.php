<?php

namespace App\Filters;

class InstrumentFilter extends QueryFilter
{
    public function search_name($search_string = '')
    {
        return $this->builder->where('name', 'LIKE', '%'.$search_string.'%' )
            ->orwhere('name_ukr', 'LIKE', '%'.$search_string.'%' );
}
}
