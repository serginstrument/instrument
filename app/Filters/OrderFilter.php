<?php

namespace App\Filters;

class OrderFilter extends QueryFilter
{
    public function search_date($search_string = '')
    {
        return $this->builder
            ->where('created_at', 'LIKE', '%'.$search_string.'%' )
            ->orwhere('first_name', 'LIKE', '%'.$search_string.'%' )
            ->orwhere('last_name', 'LIKE', '%'.$search_string.'%' );
    }
}
